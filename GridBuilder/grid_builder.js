var brushes = {
	empty: {
		display: ' ',
		export: 'x'
	},
	p: {
		display: 'p',
		export: 'p'
	},
	H: {
		display: 'H',
		export: 'H'
	},
	E: {
		display: 'E',
		export: 'E'
	},
	L: {
		display: 'L',
		export: 'L'
	}
};

class GridBuilder
{
	cells = [];
	componentIDs = {
		selector: "GB_selector",
		grid: "GB_grid",
		exportArea: "GB_export_area",
		button: "GB_export_btn",
		height: "GB_height",
		width: "GB_width",
		downloadLink: "GB_downloadLink"
	};
	height = 5;
	width = 5;
	textFile = null;

				
	constructor(targetDiv)
	{
		this.targetDiv = $('div#' + targetDiv);
		this.initialise();
		return this;
	}
	
	setHeightWidth(height, width)
	{
		this.height = height;
		this.width = width;
		this.fillGrid();
		this.resizeExportArea();
		return this;
	}
	
	resizeExportArea()
	{
		var height = 1.3 * this.height;
		var width = 1.2 * this.width;
		
		$(`#${this.componentIDs.exportArea}`).css({
			height: `${height}em`,
			width: `${width}em`
		})
	}

	initialise()
	{
		this.buildControls();
		this.initialiseGrid();
		this.setHeightWidth(this.height, this.width);
		this.buildExportArea();
	}
	
	buildControls()
	{	
		this.buildHeightWidthControls();
		this.buildSelector();
	}
	
	buildHeightWidthControls()
	{
		this.targetDiv.append(`Height: <input id='${this.componentIDs.height}' type="number" value="${this.height}"></select>`);
		this.targetDiv.append(`Width: <input id='${this.componentIDs.width}' type="number" value="${this.width}"></select>`);
	
		var gridBuilder = this;
	
		$(`#${this.componentIDs.height}`).change(function() {
			gridBuilder.setHeightWidth(this.value, gridBuilder.width);
		});
		
		$(`#${this.componentIDs.width}`).change(function() {
			gridBuilder.setHeightWidth(gridBuilder.height, this.value);
		});
	}
	
	buildSelector()
	{
		this.targetDiv.append(`Brush: <select id='${this.componentIDs.selector}'></select>`);
		var selector = $(`#${this.componentIDs.selector}`);
				
		for (let [key, value] of Object.entries(window.brushes)) {
			selector.append(`<option value='${key}'>${key}</option>`);
		}
		
		var gridBuilder = this;
		gridBuilder.selected = 'empty';
		
		selector.change(function() {
			gridBuilder.selected = $(this).val();
		});
	}
	
	initialiseGrid()
	{
		this.targetDiv.append(`<div id='${this.componentIDs.grid}'></div>`);
		this.grid = $(`#${this.componentIDs.grid}`);
	}
	
	fillGrid()
	{
		this.cells = [];
		this.grid.html('');
		
		var i, j;
		
		for(i = 0; i < this.height; i++) {
			
			var row = [];
					
			for(j = 0; j < this.width; j++) {
				row.push(new GridCell(i,j));
				this.grid.append(row[j].getHtml());
			}
			
			this.cells.push(row);
			this.grid.append('<div class="row-end"></div>');
		}
		
		this.activateCells();
	}		
	
	buildExportArea()
	{
		this.targetDiv.append(`<button id='${this.componentIDs.button}'>Convert</button>`);
		
		this.targetDiv.append(`<textarea id='${this.componentIDs.exportArea}'></textarea>`);
		this.resizeExportArea();

		this.targetDiv.append(`<br><a download="mapFile.txt" id='${this.componentIDs.downloadLink}'>Download Map</a>`);
		
		var gridBuilder = this;
		
		$(`#${this.componentIDs.button}`).click(function() {
			gridBuilder.doExport();
		});
	}
	
	activateCells()
	{
		var gridBuilder = this;
	
		$(`#${this.componentIDs.grid} .grid-cell`).mousedown(function() {
			var col = $(this).data('col');
			var row = $(this).data('row');
			
			gridBuilder.cells[row][col].set(gridBuilder.selected);
		});
	}
	
	doExport()
	{
		var csv = '';

		csv += "MDH-" + this.height + "-Hend";
		csv += '\n';
		csv += "MDW-" + this.width + "-Wend";
		csv += '\n';
		
		for (var row of this.cells) {
			for (var cell of row) {
				csv += window.brushes[cell.value].export;
			}
			csv += '\n';
		}
		csv = csv.slice(0, -1);
		$(`#${this.componentIDs.exportArea}`).text(csv);
		gridBuilder.bindDataToDownload();
	}

	makeTextFile(text)
	{
		var data = new Blob([text], {type: 'text/plain'});
		// If we are replacing a previously generated file we need to
		// manually revoke the object URL to avoid memory leaks.
        if (this.textFile !== null) {
			window.URL.revokeObjectURL(this.textFile);
		}
		this.textFile = window.URL.createObjectURL(data);
		return this.textFile;
	}

	bindDataToDownload()
	{
		var textbox = document.getElementById(this.componentIDs.exportArea);
		var link = document.getElementById(this.componentIDs.downloadLink);
		link.href = gridBuilder.makeTextFile(textbox.value);
	}
};

class GridCell
{
	constructor(i, j)
	{
		this.col = j;
		this.row = i;
		this.cellId = 'cell_' + i + '_' + j;
		this.value = 'empty';
	}
	
	getHtml()
	{
		return `<div id='${this.cellId}'
					 class='grid-cell'
					 data-col='${this.col}'
					 data-row='${this.row}'
				>${window.brushes[this.value].display}</div>`;
	}
	
	set(val)
	{
		this.value = val;
		$('#' + this.cellId).html(window.brushes[this.value].display);
	}
}