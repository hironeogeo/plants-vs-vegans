﻿using UnityEngine;
using UnityEngine.UI;

public class UIUtility : MonoBehaviour
{
    // all these fucntions were helpful for debugging purposes and will act as tools that can be used in later projects

    // sets up a new rect transform component and sets its parent to the parameter passed in
    public static RectTransform newUIElement(string name, Transform parent)
    {
        RectTransform temp = new GameObject().AddComponent<RectTransform>();
        temp.name = name;
        temp.gameObject.layer = 5;
        temp.SetParent(parent);
        temp.localScale = new Vector3(1, 1, 1);
        temp.localPosition = new Vector3(0, 0, 0);
        return temp;
    }

    // adds a text componet to a parent and object 
    public static Text newText(string text, Transform parent)
    {
        RectTransform textRect = newUIElement("Text", parent);
        Text t = textRect.gameObject.AddComponent<Text>();

        t.text = text;
        t.color = Color.black;
        t.alignment = TextAnchor.MiddleCenter;
        scaleRect(textRect, 0, 0);
        textRect.anchorMin = new Vector2(0, 0);
        textRect.anchorMax = new Vector2(1, 1);
        return t;
    }

    // transofmrs the current RectTransform's anchor points so that they match the width and height parameters passed in
    public static void scaleRect(RectTransform r, float w, float h)
    {
        r.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);

        r.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
    }
}
