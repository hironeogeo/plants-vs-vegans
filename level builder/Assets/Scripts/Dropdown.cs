﻿// Some of this code was implemented based on the tutorials of Lena Florian
// https://www.youtube.com/watch?v=nH-_ZGodtIQ
// https://www.youtube.com/watch?v=orG3Di2l-Tg&t=323s
//https://gist.github.com/lena3rika/ea2d81d4268ce309503f
//https://gist.github.com/lena3rika/2191be631404dd4889c6
//https://gist.github.com/lena3rika/df7ca76fe0e28343d6a2
// whilst the idea for the drop down list system was my own the core implementation
// of it in unity was not which is where the tutorials came in - because unity 5.2.1f does
// not have a built in UI type of drop down list. However how it has been modified and optimised 
// so that it better serves the purpose i Intended it to. please reffer to my report for more details.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// here we are also inheriting from a mouse pointer enter and exit interface to allow us to quickly set up mouse over evets.
public class Dropdown : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    // these are the class member attributes that we will use
    public GameObject buttonPrefab;
    public RectTransform container; // quite literally a container to store the child elements
    public bool isOpen; // a bool to tell us if the DDL is open or not
    public Text mainText; // the text at the top level of the DDL that we use to create our board string
    public bool isEnabled; // If the DDL is actually interactable by the user - obviously we don't want them to be trying to set values on tiles that can't contain a piece

	void Start ()
    {
        container = UIUtility.newUIElement("Container", this.transform); // sets up the container
        container.gameObject.AddComponent<VerticalLayoutGroup>(); // adds a vertical layout group so that everything can be stored vertically
        UIUtility.scaleRect(container, 0, 0); // set the scale for the container to fit the button
        container.anchorMin = new Vector2(0, 0); // clamp the anchor points again to match the button
        container.anchorMax = new Vector2(1, 0);

        container = transform.Find("Container").GetComponent<RectTransform>();

        mainText.text = ""; // initialise the main text to be an empty string to help ensure that no bad data gets left lying around

        isOpen = false; // also we want all the DDL's to start in the closed position

        int numChildren = container.childCount;

        Button childBut;

        createChildButtons();

        // creating the onclick functionality for all the child buttons
        for (int i = 0; i < numChildren; ++i)
        {
            childBut = container.GetChild(i).GetComponent<Button>();
            childBut.onClick.AddListener(cOnCLick);
        }
	}

	public void Update ()
    {
        // if the mouse pointer is hovering over the DDL it - which is how we modify the isOpen value
        // lerp the DDL's scale in the y direction which causes it to to open or close.
        Vector3 scale = container.localScale;
        scale.y = Mathf.Lerp(scale.y, isOpen ? 1 : 0, Time.deltaTime * 12);
        container.localScale = scale;
	}

    public void OnPointerEnter(PointerEventData evenData)
    {
        if(isEnabled)
            isOpen = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(isEnabled)
            isOpen = false;
    }

    // the onclick function that gets called whenever a child button is clicked.
    public void cOnCLick()
    {
        Debug.Log("made it to the on click method");
        // this string contains the text (game character value) that in the line below we are able to capture based on which button the user clicks
        string clickedChildTextVal = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text;

        //container.

        // this line navigates its way up the hierarchy from the current selected object till it finds the top level button.
        // it then accessess the text component of that top level button and assignes the value from the we captured to text 
        // attribute of the text component -  meaning that the character that was on the child button is now on the top level button
        // EventSystem.current.currentSelectedGameObject.transform.parent.parent.GetComponentInChildren<Text>().text = clickedChildTextVal;

        mainText.text = clickedChildTextVal;
    }

    void createChildButtons()
    {
        // here we get the options from the GameManager
        List<string> symbols = GameManager.Instance.getMapSymbols();
        Button tempButton;

        // read the each entry
        foreach (string s in symbols)
        {
            // create button for each entry in the file
            GameObject go = Instantiate(buttonPrefab);

            // setup the new button
            go.transform.SetParent(container, false);
            go.transform.localScale = new Vector3(1, 1, 1);

            tempButton = go.GetComponentInChildren<Button>();
            tempButton.onClick.AddListener(cOnCLick);
            tempButton.GetComponentInChildren<Text>().text = s;
        }
    }
}
