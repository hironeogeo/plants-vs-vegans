﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

// singleton class we can use to store vaariables about the game that can be accessed from anywhere
public class GameManager : MonoBehaviour {

    // declare variables
    public static GameManager instance;

    private const string symbolFilePath = "../level builder/Assets/Resources/mapSymbols.txt";             // stores the file path of the symbols file

    // setting up the singleton instance
    public static GameManager Instance
    {
        get { return instance ?? (instance = new GameObject("GameManager").AddComponent<GameManager>()); }
    }

    void Awake()
    {

    }

    // functions used to get variables stored here
    #region getters

   

    #endregion

    // functions used to update the values stored here
    #region setters

   

    #endregion

    // function to get the possible symbols from the symbols  
    // file that make the dropdown list selectable items
    public List<string> getMapSymbols()
    {
        StreamReader reader = new StreamReader(symbolFilePath);
        string mapOption = "";
        List<string> symbols = new List<string>();

        while (!reader.EndOfStream)
        {
            mapOption = reader.ReadLine();
            symbols.Add(mapOption.Split(' ')[0]);
        }
        return symbols; 
    }

    // function to save the modified  
    // "Live Symbols" To the symbols file
    public void saveMapSymbols()
    {

    }


}
