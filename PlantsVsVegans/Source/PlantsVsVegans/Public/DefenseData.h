// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DefenseData.generated.h"

USTRUCT(BlueprintType)
struct FComponentData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FString ComponentName;

	UPROPERTY(EditAnywhere)
	FString Parent = "";

	UPROPERTY(EditAnywhere)
	UStaticMesh* Mesh;

	UPROPERTY(EditAnywhere)
	FVector Offset;

};

UCLASS(BlueprintType)
class PLANTSVSVEGANS_API UDefenseData : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere)
	TArray<FComponentData> DefenseAttributes;


	TArray<FComponentData> getDefenseAttributes() const;
	
};
