﻿// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once
#include "PathfinderUtils.h"
#include <vector>
#include "CoreMinimal.h"

using namespace std;

/**
 * 
 */
//template <typename T, size_t COL, size_t ROW>
class PLANTSVSVEGANS_API PathFinder
{
private:
	int i, j, ROW, COL;
	Pair src, lastDest, currentCell, cellInvestigating;
	//array<array<T, COL>, ROW> grid;
	std::vector<std::vector<char>> newGrid;
	std::vector<Pair> cellsInvestigating;
	bool destinationFound = false;
	array<Pair, 4> directions;

	// Create a closed list and initialise it to false which
	// means that no cell has been included yet This closed
	// list is implemented as a boolean 2D array
	//bool closedList[ROW][COL];
	std::vector<std::vector<bool>> newClosedList;

	//array<array<cell, COL>, ROW> cellDetails;
	std::vector<std::vector<cell>> newCellDetails;

	/*
		Create an open list having information as-
		<f, <i, j>>
		where f = g + h,
		and i, j are the ROW and COLumn index of that cell
		Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
		This open list is implemented as a set of tuple.*/
	std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;

public:

	PathFinder();

	PathFinder(std::vector<std::vector<char>> grid, int row, int col);

	~PathFinder();

	const Pair getCurrentCell() const;

	const Pair getCellInvestigating() const;

	const std::vector<Pair> getCellsInvestigating() const;

	const auto getClosedList() const;
	const auto getCellDetails() const;
	const auto getOpenList() const;
	const bool isOpenListEmpty() const;
	const bool isDestinationFound() const;


	void getVertexAndUpdateLists();
	void popOpenList();
	void removeVertexFromClosedList(int i, int j);
	std::vector<Pair> getNeighbors(Pair currentCellPos);
	std::vector<Pair> getLoadBalancerNeighbors(Pair currentCellPos);


	auto getGrid() const;

	// contained aStar search function
	void aStarSearch(const Pair& src, const Pair& dest);

	void setup(const Pair& start, const Pair& destination);

	void tracePathForGame(std::stack<Pair>& route);
};

////template<typename T, size_t COL, size_t ROW>
///*inline*/ void PathFinder/*<T, COL, ROW>*/::popOpenList()
//{
//	openList.pop();
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ void PathFinder/*<T, COL, ROW>*/::removeVertexFromClosedList(int a, int b)
//{
//	newClosedList[a][b] = true;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ void PathFinder/*<T, COL, ROW>*/::setup()
//{
//
//	// PathFinder.src and PathFinder.dest must have been assigned for this function to be called
//
//
//	// Initialising the parameters of the starting node
//	i = src.first, j = src.second;
//	newCellDetails[i][j].f = 0.0;
//	newCellDetails[i][j].g = 0.0;
//	newCellDetails[i][j].h = 0.0;
//	newCellDetails[i][j].parent = { i, j };
//
//	/* Create an open list having information as-
//	<f, <i, j>>
//	where f = g + h,
//	and i, j are the ROW and COLumn index of that cell
//	Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
//	This open list is implemented as a set of tuple.*/
//	//std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;
//
//	// Put the starting cell on the open list and set its
//	// 'f' as 0
//	openList.emplace(0.0, i, j);
//}
//
///*inline*/ PathFinder::PathFinder()
//{
//
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest)
//{
//	//memset(newClosedList, false, sizeof(newClosedList));
//	this->src = src;
//	this->dest = dest;
//
//	assert(verifyClassIntegrity(newGrid, src, dest));
//
//}
//
////template<typename T, size_t COL, size_t ROW>
/////*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest, array<array<T, COL>, ROW> grid)
////{
////	//memset(newClosedList, false, sizeof(newClosedList));
////	this->src = src;
////	this->dest = dest;
////	this->newGrid = grid;
////
////	assert(verifyClassIntegrity(newGrid, src, dest));
////}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest, std::vector<std::vector<char>> grid)
//{
//	//memset(newClosedList, false, sizeof(newClosedList));
//	this->src = src;
//	this->dest = dest;
//	this->newGrid = grid;
//
//	assert(verifyClassIntegrity(newGrid, src, dest));
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest, char c)
//{
//	//memset(newClosedList, false, sizeof(newClosedList));
//	this->src = src;
//	this->dest = dest;
//
//	assert(verifyClassIntegrity(newGrid, src, dest));
//
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::~PathFinder()
//{
//
//}
//
//
//// A Function to find the shortest path between a given
//	// source cell to a destination cell according to A* Search
//	// Algorithm
////template<typename T, size_t COL, size_t ROW>
///*inline*/ void PathFinder/*<T, COL, ROW>*/::aStarSearch(const Pair& start, const Pair& destination)
//{
//	// We set this boolean value as false as initially
//	// the destination is not reached.
//	while (!openList.empty())
//	{
//		const Tuple& p = openList.top();
//		// Add this vertex to the closed list
//		i = get<1>(p); // second element of tupla
//		j = get<2>(p); // third element of tupla
//
//		// Remove this vertex from the open list
//		openList.pop();
//		newClosedList[i][j] = true;
//		/*
//				Generating all the 8 successor of this cell
//						N.W N N.E
//						\ | /
//						\ | /
//						W----Cell----E
//								/ | \
//						/ | \
//						S.W S S.E
//
//				Cell-->Popped Cell (i, j)
//				N --> North	 (i-1, j)
//				S --> South	 (i+1, j)
//				E --> East	 (i, j+1)
//				W --> West		 (i, j-1)
//				N.E--> North-East (i-1, j+1)
//				N.W--> North-West (i-1, j-1)
//				S.E--> South-East (i+1, j+1)
//				S.W--> South-West (i+1, j-1)
//		*/
//		for (int add_x = -1; add_x <= 1; add_x++) {
//			for (int add_y = -1; add_y <= 1; add_y++) {
//				Pair neighbour(i + add_x, j + add_y);
//				// Only process this cell if this is a valid
//				// one
//				if (isValid(newGrid, neighbour, ROW, COL)) {
//					// If the destination cell is the same
//					// as the current successor
//					if (isDestination(neighbour, dest))
//					{ // Set the Parent of the destination cell
//						newCellDetails[neighbour.first][neighbour.second].parent = { i, j };
//						printf("The destination cell is found\n");
//						//tracePath(newCellDetails, dest);
//						destinationFound = true;
//						return;
//					}
//					// If the successor is already on the
//					// closed list or if it is blocked, then
//					// ignore it. Else do the following
//					else if (!newClosedList[neighbour.first][neighbour.second]
//						&& isUnBlocked(newGrid, neighbour, ROW, COL))
//					{
//						double gNew, hNew, fNew;
//						gNew = newCellDetails[i][j].g + 1.0; // TODO: GEORGE THIS IS WHERE WE NEED TO IMPLEMENT THE COST CALCULATION
//						hNew = calculateHValue(neighbour, destination);
//						fNew = gNew + hNew;
//
//						// If it isn�t on the open list, add
//						// it to the open list. Make the
//						// current square the parent of this
//						// square. Record the f, g, and h
//						// costs of the square cell
//						//			 OR
//						// If it is on the open list
//						// already, check to see if this
//						// path to that square is better,
//						// using 'f' cost as the measure.
//						if (newCellDetails[neighbour.first][neighbour.second].f == -1
//							|| newCellDetails[neighbour.first][neighbour.second].f > fNew)
//						{
//							openList.emplace(fNew, neighbour.first, neighbour.second);
//
//							// Update the details of this
//							// cell
//							newCellDetails[neighbour.first][neighbour.second].g = gNew;
//							newCellDetails[neighbour.first][neighbour.second].h = hNew;
//							newCellDetails[neighbour.first][neighbour.second].f = fNew;
//							newCellDetails[neighbour.first][neighbour.second].parent = { i, j };
//						}
//					}
//				}
//			}
//		}
//	}
//
//	// When the destination cell is not found and the open
//	// list is empty, then we conclude that we failed to
//	// reach the destiantion cell. This may happen when the
//	// there is no way to destination cell (due to
//	// blockages)
//	printf("Failed to find the Destination Cell\n");
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const Pair PathFinder/*<T, COL, ROW>*/::getCurrentCell() const
//{
//	return currentCell;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const Pair PathFinder/*<T, COL, ROW>*/::getCellInvestigating() const
//{
//	return cellInvestigating;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const std::vector<Pair> PathFinder/*<T, COL, ROW>*/::getCellsInvestigating() const
//{
//	return cellsInvestigating;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ auto PathFinder/*<T, COL, ROW>*/::getGrid() const
//{
//	return newGrid;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getClosedList() const
//{
//	return newClosedList;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getCellDetails() const
//{
//	return newCellDetails;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getOpenList() const
//{
//	return openList;
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const bool PathFinder/*<T, COL, ROW>*/::isOpenListEmpty() const
//{
//	return openList.empty();
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ void PathFinder/*<T, COL, ROW>*/::getVertexAndUpdateLists()
//{
//	//const Tuple& p = getOpenList().top();
//	const Tuple& p = openList.top();
//
//	// Add this vertex to the closed list
//	i = get<1>(p); // second element of tupla
//	j = get<2>(p); // third element of tupla
//
//	// Remove this vertex from the open list
//	popOpenList();
//	removeVertexFromClosedList(i, j);
//}
//
////template<typename T, size_t COL, size_t ROW>
///*inline*/ const bool PathFinder/*<T, COL, ROW>*/::isDestinationFound() const
//{
//	return destinationFound;
//}