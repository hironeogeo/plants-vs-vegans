// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "StrengthsAndWeaknesses.h"
#include "MapTile.h"

#include "Enemy.generated.h"

class AEnemyStart;
class APlayerHQ;
class ALoadBalancerTile;

constexpr double distanceErrorMargin = 4.0;

USTRUCT(BlueprintType)
struct FEnemyStatsPack 
{
	GENERATED_USTRUCT_BODY()

	float defense;
	float attackRate; // frequency in seconds the enemy can attack
	int health;
	int speed;
	int damageCanInflict;
	int currencyValue;
	StrengthAndWeaknessType strongAgainst;
	StrengthAndWeaknessType weakAgainst;
};

UENUM(BlueprintType)
enum class EnemyTypes : uint8
{
	ENEMYBOSS UMETA(DisplayName = "Enemy-Boss"),
	ENEMYGRUNT UMETA(DisplayName = "Enemy-Grunt")
};

UENUM(BlueprintType)
enum class EnemySubTypes : uint8
{
	ENEMYGRUNTTYPE1 UMETA(DisplayName = "Enemy-Grunt-Type-1"),
	ENEMYGRUNTTYPE2 UMETA(DisplayName = "Enemy-Grunt-Type-2"),
	ENEMYGRUNTTYPE3 UMETA(DisplayName = "Enemy-Grunt-Type-3"),
	ENEMYBOSSTYPE1 UMETA(DisplayName = "Enemy-Boss-Type-1"),
	ENEMYBOSSTYPE2 UMETA(DisplayName = "Enemy-Boss-Type-2"),
	ENEMYBOSSTYPE3 UMETA(DisplayName = "Enemy-Boss-Type-3"),
	ENEMYSUBTYPESMAX UMETA(DisplayName = "EnemySubTypesMax")
};

UENUM(BlueprintType)
enum class EnemyGruntTypes : uint8
{
	ENEMYGRUNTTYPE1 UMETA(DisplayName = "Enemy-Grunt-Type-1"),
	ENEMYGRUNTTYPE2 UMETA(DisplayName = "Enemy-Grunt-Type-2"),
	ENEMYGRUNTTYPE3 UMETA(DisplayName = "Enemy-Grunt-Type-3"),
	ENEMYGRUNTTYPESMAX UMETA(DisplayName = "EnemyGruntTypesMAX")
};

//UENUM(BlueprintType)
enum class EnemyBossTypes : uint8
{
	ENEMYBOSSTYPE1 UMETA(DisplayName = "Enemy-Boss-Type-1") = (uint8)EnemyGruntTypes::ENEMYGRUNTTYPESMAX,
	ENEMYBOSSTYPE2 UMETA(DisplayName = "Enemy-Boss-Type-2"),
	ENEMYBOSSTYPE3 UMETA(DisplayName = "Enemy-Boss-Type-3"),
	ENEMYBOSSTYPESMAX UMETA(DisplayName = "EnemyBossTypesMAX")
};

UCLASS()
class PLANTSVSVEGANS_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

	UPROPERTY()
		USceneComponent* root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* enemyMesh;

	FEnemyStatsPack enemyStats;
	EnemyTypes enemyType;
	EnemySubTypes enemySubType;

	FRotator tmpRot;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	const bool getIsAlive() const;
	UFUNCTION(BlueprintCallable)
	void setIsAlive(bool newIsAliveState);
	void die();

	UFUNCTION()
	void attack(APlayerHQ* playerHQ);

	UFUNCTION(BlueprintCallable)
	void setEnemyType(EnemyTypes newEnemyType);

	UFUNCTION(BlueprintCallable)
	void setEnemySubType(EnemySubTypes newEnemySubType);

	UFUNCTION(BlueprintCallable)
	void setupEnemyStatusPack(FEnemyStatsPack newEnemyStatsPack);

	UFUNCTION(BlueprintCallable)
	AEnemyStart* getManager();

	UFUNCTION(BlueprintCallable)
	void setMesh(UStaticMesh* newStaticMesh);

	UFUNCTION(BlueprintCallable)
	void setManager(AEnemyStart* managerPtr);

	UFUNCTION()
	void onOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void onOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	void setRoute(TArray<FPair> newRoute);

	UFUNCTION(BlueprintCallable)
	void setCurrentTile(AMapTile* newCurrentTile);

	UFUNCTION(BlueprintCallable)
	void setTargetTile(AMapTile* newCurrentTile);

	UFUNCTION(BlueprintCallable)
	void setupEnemy(AEnemyStart* enemyStartPtr, UStaticMesh* newMesh, TArray<FPair> newRoute, AMapTile* enemyStartTilePtr);
	
	void damageEnemy(const int& damageToEnemy );
	void setDefense(const float& newEnemyDefense);
	void setSpeed(const int& newEnemySpeed);
	const bool getShouldMove() const;
	void setShouldMove(bool shouldMove);


private:
	bool isAlive;
	AEnemyStart* manager;
	UCapsuleComponent* capsuleComponent;
	bool canAttack;
	FTimerHandle attackTimer;
	FTimerDelegate timerDel;
	bool shouldMove;
	TArray<FPair> route;
	AMapTile* currentTile;
	AMapTile* targetTile;
	FVector vectorToNextTile;

	FVector playerHQLocation;
	FVector playerHQSlotLocation;
	FVector vectorToPlayerHQSlot;

	bool gotPlayerHQSlot = false;

	// do we need a bool for each of these states?
	bool movingToNextTile, reachedNextTile, movingToPlayerHQSlot, reachedPlayerHQSlot;

	void handlePlayHQCollision(class AActor* OtherActor);
	void handleBulletCollision(class AActor* OtherActor);
	
	AMapTile* findNextTargetTile();
	FVector calculateMovementVector();
	FVector generateGridAdjustedVector(const FVector& inputVector);

	bool hasReachedNextTile();
	bool hasReachedEndOfPath();
	bool hasReachedPlayerHQSlot();

	const float calculateAngleToRotate();

	APlayerHQ* playerHQPtr = nullptr;

	bool rotatedToFacePlayerHQ = false;

	void moveToPlayerHQ();
	void moveAlongAssignedPath();
	
};
