// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/StaticMeshComponent.h"
#include <string>
#include <string.h>
#include "DefenseBaseComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PLANTSVSVEGANS_API UDefenseBaseComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDefenseBaseComponent();

	UDefenseBaseComponent(const char* name, const FVector& m_pos);
	UDefenseBaseComponent(const char* name, const FVector& m_pos, const FRotator& m_rot, std::string meshPath, UDefenseBaseComponent* parent);
	void setup(const char* name, const FVector& m_pos, const FRotator& m_rot, std::string meshPath, UDefenseBaseComponent* parent);
	void setup(FString compName, FString meshPath, FVector componentOffset);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UFUNCTION(BlueprintCallable)
	FString getComponentname() const;

	UFUNCTION(BlueprintCallable)
	void setMesh(UStaticMeshComponent* newMesh);

private:
	UStaticMeshComponent* componentMesh;
	FString componentName;

	FVector m_pos;
	FRotator m_rot;
	FMatrix m_worldMat;

	UDefenseBaseComponent* parentNode;
};
