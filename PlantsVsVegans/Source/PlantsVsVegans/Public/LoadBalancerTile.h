// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "MapTile.h"
#include <vector>
#include <stack>


#include "LoadBalancerTile.generated.h"

USTRUCT(BlueprintType)
struct FRouteStats
{
	GENERATED_USTRUCT_BODY()

	int numEnemiesSent; // aka times picked
	int routeLength; // number of tiles following this one before the end
	int routeWeight; // the weighting assigned to each route

	FRouteStats()
	{
		numEnemiesSent = 0;
		routeLength = 0;
		routeWeight = 0;
	}

	FRouteStats(const int& newRouteLength)
	{
		numEnemiesSent = 0;
		routeLength = newRouteLength;
		routeWeight = 0;
	}

};

USTRUCT(BlueprintType)
struct FLoadBalancerRouteStruct
{
	GENERATED_USTRUCT_BODY()

	FRouteStats routeStats;
	std::stack<std::pair<int, int>> loadBalancerRoute;

	FLoadBalancerRouteStruct()
		:routeStats()
	{
		
	}


	FLoadBalancerRouteStruct(FRouteStats stats, std::stack<std::pair<int, int>> route)
	{
		routeStats = stats;
		loadBalancerRoute = route;
	}

};

/**
 * 
 */
UCLASS()
class PLANTSVSVEGANS_API ALoadBalancerTile : public AMapTile
{
	GENERATED_BODY()


public:
	ALoadBalancerTile();
	~ALoadBalancerTile();
	std::stack<std::pair<int, int>> getPreferedRoute();
	void addRoute(std::stack<std::pair<int, int>> newRoute);
	void setLoadBalanerRoutes(std::vector<std::stack<Pair>> routes);
	// to be called as part of the setup of this class
	void setupWeightedRoutes();
	void updatePreferredRoute();

	// function to generate a route for an enemy (based on weightings)
	TArray<FPair> generateEnemyRouteByWeight();

	// function to generate a random enemy route
	TArray<FPair> generateEnemyRouteRandom();

private:
	std::vector<std::stack<std::pair<int, int>>> loadBalancerRoutes;
	std::stack<std::pair<int, int>> preferedRoute;
	std::vector<FRouteStats*> routeStats;

	std::vector<FLoadBalancerRouteStruct> weightedRoutes;

	// Update the routes with the stats after interacting with an enemy (post setup)
	void calculateRouteWeights();

//	std::stack<std::pair<int, int>> findLowestWeightRoute();

	bool interactedWithAnEnemy;
	

	
};
