// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerCurrency.generated.h"

// Delegate that acts as or message sending system to rest of the game that the UI needs updating
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCurrencyHUDUpdate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PLANTSVSVEGANS_API UPlayerCurrency : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerCurrency();

	UFUNCTION(BlueprintCallable)
	void deductFunds(const int& fundsToDeduct);

	UFUNCTION(BlueprintCallable)
	void addFunds(const int& fundsToAdd);

	UFUNCTION(BlueprintCallable)
	void setFunds(const int& fundsValue);

	UFUNCTION(BlueprintCallable)
	const int getFunds();

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FCurrencyHUDUpdate currencyHUDUpdate;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	int playerFunds;
		
};
