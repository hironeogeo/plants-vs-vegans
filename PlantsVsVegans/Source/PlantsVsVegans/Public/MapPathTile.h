// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "MapTile.h"
#include "MapPathTile.generated.h"

/**
 * 
 */
UCLASS()
class PLANTSVSVEGANS_API AMapPathTile : public AMapTile
{
	GENERATED_BODY()

public:
	AMapPathTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
