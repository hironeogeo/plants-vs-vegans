// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once
#include <string>
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "CoreMinimal.h"

/**
 * 
 */
class PLANTSVSVEGANS_API Component
{
public:
	Component();
	Component(const char* name, const FVector& m_pos);
	Component(const char* name, const FVector& m_pos, const FRotator& m_rot, std::string meshPath, Component* parent);
	~Component();
	FString getComponentname() const;
	void update();

private:
	UStaticMeshComponent * componentMesh;
	
	FString componentName;

	FVector m_pos;
	FRotator m_rot;
	FMatrix m_worldMat;

	Component* parentNode;
	
};
