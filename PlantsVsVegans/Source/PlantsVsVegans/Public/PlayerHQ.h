// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "MapTile.h"
#include "PlayerCurrency.h"
#include "PlayerHQ.generated.h"

// Delegate that acts as or message sending system to rest of the game that the player has died
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerHasDied);

// Delegate that acts as or message sending system to rest of the game that the UI needs updating
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerHealthUpdateRequired);

/**
 * 
 */
UCLASS()
class PLANTSVSVEGANS_API APlayerHQ : public AMapTile
{
	GENERATED_BODY()

public:
	APlayerHQ();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//const int getPlayerHealth() const;
	const float getPlayerDefenseFactor() const;
	void applyDamage(int damageToSubtract);
	const bool isPlayerHQAlive() const;

	UFUNCTION(BlueprintCallable)
	void setMesh(UStaticMesh* newMesh);

	UFUNCTION(BlueprintCallable)
	void setPlayerCurrency(UPlayerCurrency* newPlayerCurrency);

	UFUNCTION(BlueprintCallable)
	const int getPlayerFunds();

	UFUNCTION(BlueprintCallable)
	const int getPlayerHealth();

	UFUNCTION(BlueprintCallable)
	void deductFunds(int fundsToDeduct);

	UFUNCTION(BlueprintCallable)
	UPlayerCurrency* getCurrencyComponent();

	UFUNCTION(BlueprintCallable)
	void broadcastGameOver();

	UFUNCTION(BlueprintCallable)
	void generatePlayerHQEnemySpots();

	FVector getRandomSlot();

	UPROPERTY()
	UPlayerCurrency* playerCurrency;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FPlayerHasDied playerHasDied;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FPlayerHealthUpdateRequired playerHealthUpdateRequired;

	// The number of enemy slots / accessible positions round the PlayerHQ
	const uint8 numberOfSlots = 12;

	private:
	// variable to store health of the player
	int playerHealth;
	// variable to control the defense of the player and affects the amount of damage the player takes
	float playerDefenseFactor;

	int playerFunds;
	bool playerHQALive;

	TArray<FVector> slots;


};
