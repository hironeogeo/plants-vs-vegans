// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once
class AMapTile;
class ALoadBalancerTile;
class AEnemyStart;
class APlayerHQ;
class PathFinder;
class GameObjectComponent;
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <string>
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "GameObjectComponent.h"
#include "Components/StaticMeshComponent.h"
#include "LevelData.h"
#include <vector>
#include <stack>
#include <map>

#include "AMapManager.generated.h"

typedef std::pair<int, int> Pair;

USTRUCT(BlueprintType)
struct FLoadBalancerProcessingUnit
{
	GENERATED_USTRUCT_BODY()

public:
	ALoadBalancerTile* loadBalancerTile;
	Pair loadBalancerTileLocation;
	std::vector<Pair> cellNeighbores;

	FLoadBalancerProcessingUnit()
	{
		loadBalancerTile = nullptr;
		loadBalancerTileLocation = Pair(-1, -1);

	}
	FLoadBalancerProcessingUnit(std::pair<int, int> tileLocation, ALoadBalancerTile* tile, std::vector<Pair> neighbores )
	{ 
		loadBalancerTile = tile;
		loadBalancerTileLocation = tileLocation;
		cellNeighbores = neighbores;
	}
};

// structure containing id of loadBalancingCellPtr (parent) and location of the neighbor cell
USTRUCT(BlueprintType)
struct FLoadBalancerNeighborProcessingUnit
{
	GENERATED_USTRUCT_BODY()

public:
	// int reference to the loadBalancerCell used for lookup
	int loadBalancerCellParent;
	Pair neighborLocation;
	FLoadBalancerNeighborProcessingUnit()
	{
		loadBalancerCellParent = -1;
		neighborLocation = Pair(-1, -1);
	}
	FLoadBalancerNeighborProcessingUnit(int cellParent, Pair location)
	{
		loadBalancerCellParent = cellParent;
		neighborLocation = location;
	}
};

struct InterProcessingUnit
{
public:
	Pair parent;
	Pair neighborCell;

	InterProcessingUnit()
	{
		parent = { Pair(-1,-1) };
		neighborCell = { Pair(-1,-1) };
	}

	InterProcessingUnit(Pair newParent, Pair newNeighborCell)
	{
		parent = newParent;
		neighborCell = newNeighborCell;
	}
};

struct ProcessingUnit
{
	Pair parent;
	Pair start;
	Pair dest;

	ProcessingUnit()
	{
		parent = { Pair(-1,-1) };
		start = { Pair(-1,-1) };
		dest = { Pair(-1,-1) };
	};

	ProcessingUnit(Pair newStart, Pair newDest)
	{
		parent = { Pair(-1,-1) };
		start = newStart;
		dest = newDest;
	};

	ProcessingUnit(Pair newParent, Pair newStart, Pair newDest)
	{
		parent = newParent;
		start = newStart;
		dest = newDest;
	};
};


USTRUCT(BlueprintType)
struct FPair
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int x;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int y;

	FPair() { x = -1; y = -1; }
	FPair(int newX, int newY) { x = newX; y = newY; }
	Pair toPair() { return Pair(x, y); };
};

UENUM(BlueprintType)
enum class MapTileCategories : uint8
{
	STANDARD UMETA(DisplayName = "Standard"),
	ENEMYSTART UMETA(DisplayName = "EnemyStart"),
	PLAYERSTART UMETA(DisplayName = "PlayerStart"),
	LOADBALANCER UMETA(DisplayName = "LoadBalancer"),
	PATH UMETA(DisplayName = "Path"),
	UNKNOWN UMETA(DisplayName = "Unkown"),
};

UCLASS()
class PLANTSVSVEGANS_API AAMapManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAMapManager();
	~AAMapManager();

	UPROPERTY(EditAnywhere, category = manager)
		UStaticMesh* managerMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TEnumAsByte<MapTileCategories> enumValues;

	TArray<MapTileCategories> mapEnum;

	TArray<FPair> enemyRouteGameVersion;

	// read in a map file and assemble the level 
	UFUNCTION(BlueprintCallable)
	void buildLevelFromMapFile();

	UFUNCTION(BlueprintCallable)
	TArray<MapTileCategories> getMapEnumArray();

	TArray<FPair> generateEnemyRoute();

	TArray<FPair> generateEnemyRoute(std::stack<Pair> enemyRoute);

	UFUNCTION(BlueprintCallable)
	void getEnemyRoutes(AEnemyStart* enemyStart);

	UFUNCTION(BlueprintCallable)
	void getLoadBalancerRoutes(ALoadBalancerTile* loadBalancer);
	
	UFUNCTION(BlueprintCallable)
	int getMapHeight();
	UFUNCTION(BlueprintCallable)
	int getMapWidth();

	UFUNCTION(BlueprintCallable)
	void addMapTilesGridElement(AMapTile* mapTile);

	UFUNCTION(BlueprintCallable)
	int twodToOneDGridConversion(FPair twoDGridDimmention);

	UFUNCTION(BlueprintCallable)
	FPair onedToTwoDGridConversionFPair(int counter);

	Pair onedToTwoDGridConversionPair(int counter);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int playerOnBoardThreshhold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int enemiesToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int maxEnemiesAlive;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AMapTile*> mapTilesGrid;

	// does it make sense to have a container of the important tile here
	// in the map manager class rather than having to look them up all the time
	// may make processing easier at the cost of a larger class?

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<APlayerHQ*> playerHQTilesGrid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AEnemyStart*> enemyStartTilesGrid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ALoadBalancerTile*> loadBalancerTilesGrid;

private:
	void parseMapFile(FString mapFileString);
	const MapTileCategories generateMapTileEnum(TCHAR mapStringParam);

	unsigned short int heightCoutner;

	// height dimension of the map 
	unsigned short int mapHeight;
	// width dimension of the map
	unsigned short int mapWidth;

	UPROPERTY(EditAnywhere)
	ULevelData* MapData;

	// string containing the map characters
	FString mapString;
	// character array containing the map
	PathFinder* pathfinder;
	std::vector<Pair> startLocations;
	std::vector<Pair> destinationLocations;
	std::vector<Pair> loadBalancerLocations;
	std::vector<std::vector<char>> mapGrid;
	std::stack<Pair> enemyRoute;
	std::map<Pair, std::vector<std::stack<Pair>>> routes;

	// Generate the combinations between the enemy start and load balancer tiles to be destinations
	std::vector<ProcessingUnit*> createCombinations();
	// Test the potential route combinations and store the valid routes
	void testCombinations(std::vector<ProcessingUnit*> combinations);
	std::vector<std::stack<Pair>> getRoutesByCellLocation(Pair cellLocation);
	bool isParentSet(ProcessingUnit* processingUnit);

	// Check that the route doesn't have duplicate cells (ie going back on it's self)
	bool checkRouteValid(std::stack<Pair> routeToCheck);
};