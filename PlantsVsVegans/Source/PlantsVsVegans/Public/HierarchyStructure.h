// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once
#include <string>
#include <map>
#include <vector>
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "GameObjectComponent.h"
#include "DefenseData.h"
#include "CoreMinimal.h"

class ADefense;

/**
 * 
 */
class PLANTSVSVEGANS_API HierarchyStructure
{
public:
	HierarchyStructure();
	HierarchyStructure(FString name);
	HierarchyStructure(FString name, UWorld* worldPtr, ADefense* defensePtr, UDefenseData* defenseDataPtr);
	~HierarchyStructure();

	void readInFromFile();// does what it says on the tin, reads in the data from a given file path
	AGameObjectComponent* findComponent(FString criteria); // searches through the tree to find a component starting at the top
	std::map<FString, AGameObjectComponent*> getMap();
	std::vector<FString> getComponentOrder() const;
	FString getHierarchyName() const;
	TArray<AGameObjectComponent*> getComponents();

private:
	FString hierarchyName;
	UWorld* world;
	ADefense* defenseParent;

	UPROPERTY()
	UDefenseData* defenseData;

	std::map<FString, AGameObjectComponent*> structure;
	std::vector<FString> componentOrder;
};
