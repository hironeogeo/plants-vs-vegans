// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "LevelData.generated.h"

/**
 * 
 */

// probably should be a struct if everything is public
UCLASS()
class PLANTSVSVEGANS_API ULevelData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	int MapHeight;

	UPROPERTY(EditAnywhere)
	int MapWidth;

	UPROPERTY(EditAnywhere)
	FString MapString;

	bool IsLevelDataValid() const;
	
};
