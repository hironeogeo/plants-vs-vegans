// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "MapTile.h"
#include "Enemy.h"
#include <algorithm>
#include "Math/UnrealMathUtility.h"
#include "PlayerCurrency.h"

#include "EnemyStart.generated.h"

// Delegate that acts as or message sending system to rest of the game that the player has won
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerHasWon);

// Delegate that acts as or message sending system to rest of the game an enemy has died
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyHasDied);

/**
 * 
 * EnemyStart - EnemyManager class responsible for managing the enemies
 *
 */
UCLASS()
class PLANTSVSVEGANS_API AEnemyStart : public AMapTile
{
	GENERATED_BODY()

public:
		AEnemyStart();

		void reportEnemyDeath(AEnemy* enemy);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	const float getDurationBetweenEnemySpawn();

	UFUNCTION(BlueprintCallable)
	const bool getStandardEnemyCapacityRemaining();

	UFUNCTION(BlueprintCallable)
	const bool getSpawnCapacityAvaliable();

	UFUNCTION(BlueprintCallable)
	const int getNumberOfEnemiesLeftToSpawn();

	UFUNCTION(BlueprintCallable)
	const bool getReachedMaxEnemiesAlive();

	UFUNCTION(BlueprintCallable)
	AEnemy* getNewEnemyCoreForSpawn();

	UFUNCTION(BlueprintCallable)
	AEnemy* generateEnemy(EnemyTypes typeOfEnemy);

	UFUNCTION(BlueprintCallable)
	TArray<FPair> getEnemyRoute();

	const EnemySubTypes generateGruntSubtype();
	const EnemySubTypes generateBossSubtype();

	FEnemyStatsPack generateEnemyStats(const EnemySubTypes& enemyType);

	void reduceEnemySpawningTime();

	UFUNCTION(BlueprintCallable)
	const FVector getEnemySpawnLocation();

	UFUNCTION(BlueprintCallable)
	void setEnemySpawnLocation(const FVector& newSpawnLocation);

	UFUNCTION(BlueprintCallable)
	void decativateAllEnemies();

	UFUNCTION(BlueprintCallable)
	void setPlayerCurrency(UPlayerCurrency* newPlayerCurrency);

	UFUNCTION(BlueprintCallable)
	void setEnemyStartStats(int numEnemiesToSpawn, int maxNumEnemiesAlive);

	UFUNCTION(BlueprintCallable)
	void setupEnemyStart(int numEnemiesToSpawn, int maxNumEnemiesAlive);

	UFUNCTION(BlueprintCallable)
	TArray<FPair> getFullEnemyRoute();
	
	UFUNCTION(BlueprintCallable)
	FPair getEnemyRouteStart();

	UFUNCTION(BlueprintCallable)
	void removeRouteElementByIndex(int index);

	//UFUNCTION(BlueprintCallable)
	//void setManager(AAMapManager* managerPtr);

	// a function to adjust the stats of the enemies
	// generated based on the progress of the player
	void inflateEnemyStats(FEnemyStatsPack& enemyStats);

	UPROPERTY()
	UPlayerCurrency* playerCurrency;
	
	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FPlayerHasWon playerHasWon;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FEnemyHasDied enemyHasDied;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AMapTile* enemyStartTile;

	void setEnemyRoutes(std::vector<std::stack<Pair>> routes);

private:
	// The maximum number of enemies in total allowed (including the boss at the end)
	int maxNunberOfEnemies;

	// The maximum number of enemies that can be alive at any one time
	int maxAlive;

	// The total number of enemies that have been spawned
	int numEnemiesSpawned;

	// The total number of enemies that are alive at any one time
	int numAliveEnemies;

	// The duration between enemies spawning when avaliable
	float durationBetweenEnemySpawn;

	// The variable designed to slowly reduce the time between enemies being spawned
	float spawnReducer;

	// The variable required for controling the incremrnts the spwan reducer changes by
	const float spawnReducerIncrements = 0.2f;

	// The variable required for controlling the minumum spwan time for enemies
	const float minEnemySpawnTIme = 3.0f;

	// The variable designed to slowly increase the stats
	float statIncreaser;

	// The location where the enemies will appear in game
	FVector spawnLocation;

	// A vector containing the enemies avaliable for use in the game at anyone time
	std::vector<AEnemy*> enemies;

	// A variable to should be true if the number of alive enemies == the max alive
	bool spawnCapacityAvaliable;

	// The standard enemy capacity == the maxNunberOfEnemies - 1 (as the final enemy should be a boss)
	bool stdEnemyCapacityRemaining;

	// function for internal use only to find a dead enemy that can be used for a new spawning
	AEnemy* findAvaliableEnemy();

	// function for internal use only to take the newly killed enemy and swap it with the start enemy (pre optimisation?)
	void recycleEnemy(AEnemy* enemyToRecycle);

	// Path object that the enemies should follow
	TArray<FPair> enemyRoute;

	std::vector<std::stack<Pair>> enemyRoutes;

};
