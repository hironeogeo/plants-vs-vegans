// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "defense.h"
#include "GameObjectComponent.generated.h"

UCLASS()
class PLANTSVSVEGANS_API AGameObjectComponent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameObjectComponent();
	~AGameObjectComponent();
	void setup(FString compName, UStaticMesh*  mesh, FVector componentOffset, AGameObjectComponent* parentComp);

	UPROPERTY()
		USceneComponent* root;

	UPROPERTY(EditAnywhere, category = componentMesh)
		UStaticMeshComponent* componentMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// unique gameobject update function that is called periodically from each component
	void gameObjectUpdate();

	FRotator getComponentRotation();
	FVector	getComponentLocation();
	FTransform getWorldTransform();

	void updateComponentLocationWithIncrement(FVector incrementalLocationChange);
	void updateComponentRotaionWithIncrement(FRotator incrementalRotationChange);
	void updateComponentRotaion(FRotator incrementalRotationChange);
	void rotateComponentRoundZAxis(int amountToRotate);

	FVector getRightVectorFromLocation();
	FVector getRightVectorEnd();

private:
	FString componentName;
	FString meshName;
	FVector componentOffset;
	FRotator componentRotation;
	AGameObjectComponent* parent;
	FTransform worldTransform;
	FTransform localTransform;
	FQuat componentQuaternion;

};
