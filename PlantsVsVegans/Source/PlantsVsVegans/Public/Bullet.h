// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "StrengthsAndWeaknesses.h"
#include "Bullet.generated.h"

UCLASS()
class PLANTSVSVEGANS_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

	UPROPERTY()
		USceneComponent* root;

	UPROPERTY(EditAnywhere, category = bulletMesh)
		UStaticMeshComponent* bulletMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void setup(FTransform barrelTransform, FVector forwardVector, float speed, StrengthAndWeaknessType bulletType);

	UFUNCTION()
	void die();

	UFUNCTION()
	StrengthAndWeaknessType getBulletType();

private:
	bool isAlive;
	StrengthAndWeaknessType bulletType;
	FTransform worldTransform;
	FVector speed;
	FVector defenseDirection;
};
