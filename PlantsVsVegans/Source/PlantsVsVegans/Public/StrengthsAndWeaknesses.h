// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StrengthsAndWeaknesses.generated.h"

UENUM(BlueprintType)
enum class StrengthAndWeaknessType : uint8
{
	STANDARD UMETA(DisplayName = "Standard"),
	FIRE UMETA(DisplayName = "Fire"),
	WATER UMETA(DisplayName = "Water"),
	SOYAMILK UMETA(DisplayName = "Soya-Milk"),
	MUSHROOMS UMETA(DisplayName = "Mushrooms"),
	NUTS UMETA(DisplayName = "Nuts"),
	CHILLI UMETA(DisplayName = "Chilli"),
	NONE UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class Effectiveness : uint8
{
	STRONG UMETA(DisplayName = "Strong"),
	NORMAL UMETA(DisplayName = "Normal"),
	WEAK UMETA(DisplayName = "Weak")
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PLANTSVSVEGANS_API UStrengthsAndWeaknesses : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStrengthsAndWeaknesses();

	UFUNCTION()
	static Effectiveness resolveBulletEnemyCollision(StrengthAndWeaknessType enemyStrong, StrengthAndWeaknessType enemyWeak, StrengthAndWeaknessType bulletType);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
