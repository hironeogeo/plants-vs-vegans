// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include <vector>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AMapManager.h"


#include "Components/StaticMeshComponent.h"

#include "MapTile.generated.h"

typedef std::pair<int, int> TilePair;

UCLASS()
class PLANTSVSVEGANS_API AMapTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapTile();
	AMapTile(AAMapManager* mngr, const bool& isPathVal);

	UPROPERTY()
		USceneComponent* root;

	UPROPERTY(EditAnywhere, category = mapTile)
		UStaticMeshComponent* tileMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool isPath;
	bool isLoadBalancer;
	FPair tilePosition;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		const bool getIsPath();

	UFUNCTION(BlueprintCallable)
		const bool getIsLoadBalancer();
	
	UFUNCTION(BlueprintCallable)
		AAMapManager* getManager();

	UFUNCTION(BlueprintCallable)
	void setManagerPtr(AAMapManager* managerPtr);

	UFUNCTION(BlueprintCallable)
	void setTilePos(FPair newPos);

	const FVector getAdjustedLocation() const;
	FPair getTilePosition();
	
private:

	AAMapManager* manager;

};
