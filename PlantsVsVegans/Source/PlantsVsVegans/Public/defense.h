// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "HierarchyStructure.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Camera/CameraComponent.h"
#include "StrengthsAndWeaknesses.h"
#include <DefenseData.h>
#include "defense.generated.h"

// Delegate that acts as or message sending system to rest of the game 
// that a second has elapsed and the on screen timer needs updating
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerOnBoardSecondElapsed);

// Delegate that acts as or message sending system to rest of the game 
// that the time limit for player on board has finished
UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerOnBoardFinished);

UENUM(BlueprintType)
enum class DefenseType : uint8
{
	Type1 UMETA(DisplayName = "Type1"),
	Type2 UMETA(DisplayName = "Type2"),
	Type3 UMETA(DisplayName = "Type3"),
};

USTRUCT(BlueprintType)
struct FDefenseStatsPack
{
	GENERATED_USTRUCT_BODY()

	int defenseCost;
	int range;
	int fireRate;
	int firePower;
	StrengthAndWeaknessType ammoType;
	// the location where the bullets will spawn (ie end of the barrel)
	float firingOffset;
};


USTRUCT(BlueprintType)
struct FDefenseComponents
{
	GENERATED_USTRUCT_BODY()

	static const char* defenseRoot;
	static const char* defenseBase;
	static const char* defenseStem;
	static const char* defenseHead;
	static const char* defenseBarrel;
};

constexpr int MAX_BULLETS = 10;

UCLASS()
class PLANTSVSVEGANS_API ADefense : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADefense();
	~ADefense();

	UPROPERTY()
		USceneComponent* root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* defenseMesh;

	UPROPERTY(VisibleAnywhere, Category = "DefensePropertires")
		class USphereComponent* detectionZone;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "DefensePropertires")
		TArray<AGameObjectComponent*> components;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* camera;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
		FPlayerOnBoardSecondElapsed playerOnBoardSecondElapsed;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
		FPlayerOnBoardFinished playerOnBoardFinished;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void setMesh(UStaticMesh* newStaticMesh);

	UFUNCTION(BlueprintCallable)
	void setupDefense(const DefenseType& type, const int& defenseCost, UDefenseData* defenseData);

	FDefenseStatsPack generateDefenseStats(DefenseType type);

	UFUNCTION(BlueprintCallable)
	DefenseType getDefenseType() const;

	UFUNCTION(BlueprintCallable)
	FDefenseStatsPack getDefenseStats() const;

	UFUNCTION(BlueprintCallable)
	int getDefenseCost() const;

	UFUNCTION()
	void onBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void onEndOverlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//UFUNCTION()
	//void rotateDefense();

	UFUNCTION()
	const float calculateAngleToRotate();

	UFUNCTION()
	// calculates the magnitude of the vector between the enemy location and the head component of the defense
	const float calculateDistanceBetweenEnemyAndDefense(const FVector& enemyLoc);
	
	UFUNCTION()
	// returns a ptr to the closest enemy from those currently in range
	// returns nullptr if there are no enemies currently in range
	class AEnemy* findClosestEnemy();

	UFUNCTION()
	// Generates a bullet and fires it towards the targetted enemy
	void fire();

	UFUNCTION()
	// Sets the defense to be able to fire again
	void enableFiring();

	UFUNCTION()
	void setupBullet(ABullet* bulletToSetup);

	UFUNCTION(BlueprintCallable)
	void setIsPossessed();

	UFUNCTION(BlueprintCallable)
	void setNotPossessed();

	void turn(float val);

	void fireClick();

	void setRotate(bool newRotateState);

	UFUNCTION(BlueprintCallable)
	const int getElapsedSecondsCount();

	UFUNCTION(BlueprintCallable)
	const int getPlayerOnBoardTimer();

	UFUNCTION(BlueprintCallable)
	const int calculatePOBTimerValueForDisplay();

private:

	// flag to control if the defense should rotate
	bool shouldRotate;

	// flag to control if the defense is able to fire
	bool canFire;

	// flag to control if the defense is being possessed by the player
	bool isPossessed;

	// the speed the defense head will rotate to point at an enemy
	float rotationSpeed;

	// the marker to help determine if a new second has passed yet
	float newSecondMarker;

	// the maximum amount of time in seconds that the player can remain on board the defense
	int playerOnBoardTimer;

	// count of how many seconds have elapsed while the player is on board
	int elapsedSecondCount;

	// enmum value that defenses the type of defense this is
	DefenseType defenseType;

	// stats pack for the defense
	FDefenseStatsPack stats;

	// rotator to store the rotation of the head component
	FRotator headRot;

	// timer to control measure the time before the defense can fire again
	FTimerHandle fireTimer;

	// timer delegate to respond when the defense can fire again
	FTimerDelegate timerDel;
	
	// container that manages the GameObjectComponents that make up the defense
	HierarchyStructure* structure;
	
	// ptr to the enemy we are currently targetting
	class AEnemy* targetedEnemy;
	
	// container of enemies that are currently in range of the defense
	std::vector<AEnemy*> enemiesInRange;

	// container of the bullets we are able to fire from the turret
	std::vector<ABullet*> bullets;
};
