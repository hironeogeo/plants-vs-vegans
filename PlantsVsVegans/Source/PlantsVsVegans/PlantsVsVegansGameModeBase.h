// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlantsVsVegansGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PLANTSVSVEGANS_API APlantsVsVegansGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
