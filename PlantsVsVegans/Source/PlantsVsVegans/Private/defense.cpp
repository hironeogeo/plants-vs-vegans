// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "defense.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/InputComponent.h"
#include "Enemy.h"
#include "Bullet.h"
#include <limits>


const char* FDefenseComponents::defenseRoot = "root";
const char* FDefenseComponents::defenseBase = "Base";
const char* FDefenseComponents::defenseStem = "Stem";
const char* FDefenseComponents::defenseHead = "Head";
const char* FDefenseComponents::defenseBarrel = "Barrel";

// Sets default values
ADefense::ADefense()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;
	root->SetWorldLocation(GetActorLocation());

	defenseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));     
	defenseMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);

	detectionZone = CreateDefaultSubobject<USphereComponent>(TEXT("<DetectionZone>"));
	detectionZone->InitSphereRadius(0);
	detectionZone->SetupAttachment(root);

	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	camera->SetupAttachment(RootComponent);
	FVector loc = GetActorLocation();
	camera->SetRelativeLocation(FVector(loc.X - 100.0f, loc.Y + 0.0f, loc.Z + 50.0f));

	stats.defenseCost = 0;
	stats.range = 0;
	stats.fireRate = 0;

	defenseType = DefenseType::Type1;
	headRot = FRotator(0.0f, 0.0f, 0.0f);
	rotationSpeed = 10.0f;

	// setup starting general flags for the defense
	setRotate(false);
	canFire = true;
	isPossessed = false;
	bUseControllerRotationYaw = false;
	targetedEnemy = nullptr;
	
	// setup the container to store enemies in the detection zone
	enemiesInRange.clear();
	enemiesInRange.reserve(3);
	bullets.reserve(MAX_BULLETS + 1);
	
	// setup the cooldown timer when the defence will auto fire at enemies
	timerDel.BindUFunction(this, FName("enableFiring"));

	// set the player on board variables
	playerOnBoardTimer = 10;
	newSecondMarker = 0.0f;
	elapsedSecondCount = 0;

	structure = nullptr;
}

ADefense::~ADefense()
{
	if (structure != nullptr)
	{
		delete structure;
		structure = nullptr;
	}
}

// Called when the game starts or when spawned
void ADefense::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("inside beginPlay()"));
	//DrawDebugSphere(GetWorld(), GetActorLocation(),300, 50, FColor::Blue, true,-1,0,2);
}

// Called every frame
void ADefense::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (shouldRotate && targetedEnemy != nullptr)
	{
		const float amount2Rotate = calculateAngleToRotate();		
		structure->findComponent("Head")->rotateComponentRoundZAxis(FMath::Lerp(0.0f,amount2Rotate,0.15f));
		setRotate(false);
		targetedEnemy = findClosestEnemy();
	}

	// in order to keep the defense tracking the targeted enemy
	if (targetedEnemy != nullptr && !isPossessed)
	{
		setRotate(true);

		if (canFire)
		{
			fire();
		}
	}
	
	// update all the defense components in order
	if (structure != nullptr)
	{
		for (const auto& param : structure->getComponentOrder())
		{
			structure->findComponent(param)->gameObjectUpdate();
		}
	}

	if (isPossessed)
	{
		AGameObjectComponent* head = structure->findComponent(FDefenseComponents::defenseHead);
		FRotator rot2 = head->GetActorRotation();
		SetActorRotation(FRotator(0.0f, rot2.Yaw + 90.0f, 0.0f));

		newSecondMarker += GetWorld()->DeltaTimeSeconds;
		if (newSecondMarker >= 1.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("new second has elapsed"));
			// at least 1 second has elapsed 
			elapsedSecondCount += 1;
			newSecondMarker = 0.0f;

			// tell the screen to update
			playerOnBoardSecondElapsed.Broadcast();

			if (elapsedSecondCount >= playerOnBoardTimer)
			{
				// we have reached the end of player on baord
				
				// eject the player
				playerOnBoardFinished.Broadcast();

				// reset all our counts
				newSecondMarker = 0.0f;
				elapsedSecondCount = 0;
			}
		}
	}
}

// Called to bind functionality to input
void ADefense::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis("Turn", this, &ADefense::turn);

	PlayerInputComponent->BindAction("FireClick", IE_Pressed, this, &ADefense::fireClick);

}

void ADefense::setMesh(UStaticMesh* mesh)
{
	defenseMesh->SetStaticMesh(mesh);
}

void ADefense::setupDefense(const DefenseType& type, const int& defenseCost, UDefenseData* defenseData)
{
	UE_LOG(LogTemp, Warning, TEXT("inside setupDefense()"));

	defenseType = type;
	stats = generateDefenseStats(type);
	stats.defenseCost = defenseCost;

	detectionZone->SetSphereRadius(stats.range);

	const TEnumAsByte<DefenseType> defenseEnum = type;
	FString tmpDefenseTypeAsString = UEnum::GetValueAsString(defenseEnum.GetValue());

	int test = (int)type;
	test += 1;
	FString DefenseType = "DefenseType";

	FString stdDefenseTypeAsString = DefenseType + FString::FromInt(test);
	UE_LOG(LogTemp, Warning, TEXT("Defense type = %s"), *stdDefenseTypeAsString);

	structure = new HierarchyStructure(stdDefenseTypeAsString, GetWorld(), this, defenseData);
	structure->readInFromFile();
	structure->findComponent("Root")->SetActorLocation((GetActorLocation()));
	structure->findComponent("Root")->updateComponentLocationWithIncrement(GetActorLocation());

	components = structure->getComponents();

	// setup the detection zone overlap and end bindings only once the defense has been fully setup
	// - this can't be before the HierarchyStructure has been created because that's needed to resolve the collisions
	detectionZone->OnComponentBeginOverlap.AddDynamic(this, &ADefense::onBeginOverlap);
	detectionZone->OnComponentEndOverlap.AddDynamic(this, &ADefense::onEndOverlap);
}

FDefenseStatsPack ADefense::generateDefenseStats(DefenseType type)
{
	FDefenseStatsPack newStats;

	switch (type) {
	case DefenseType::Type1:
		newStats.fireRate = 1;
		newStats.range = 300;
		newStats.firePower = 100;
		newStats.firingOffset = 105.5;
		newStats.ammoType = StrengthAndWeaknessType::STANDARD;
		break;
	case DefenseType::Type2:
		newStats.fireRate = 2;
		newStats.range = 50;
		newStats.firePower = 100;
		newStats.firingOffset = 105.5;
		newStats.ammoType = StrengthAndWeaknessType::WATER;
		break;
	case DefenseType::Type3:
		newStats.fireRate = 3;
		newStats.range = 150;
		newStats.firePower = 100;
		newStats.firingOffset = 105.5;
		newStats.ammoType = StrengthAndWeaknessType::SOYAMILK;
		break;
	}
	return newStats;
}

DefenseType ADefense::getDefenseType() const
{
	return defenseType;
}

FDefenseStatsPack ADefense::getDefenseStats() const
{
	return stats;
}

int ADefense::getDefenseCost() const
{
	return stats.defenseCost;
}

void ADefense::onBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);
	if(enemy)
	{
		// if the container doesn't already contain the enemy then add it
		if (std::find(enemiesInRange.begin(), enemiesInRange.end(), enemy) == enemiesInRange.end())
		{
			enemiesInRange.push_back(enemy);
		}
		
		targetedEnemy = targetedEnemy == nullptr ? enemy : findClosestEnemy();
		setRotate(true);
	}
}

void ADefense::onEndOverlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);
	if (enemy)
	{
		// we need to remove the enemy from our container
		const auto iter = std::find(enemiesInRange.begin(), enemiesInRange.end(), enemy);
		if (iter != enemiesInRange.end())
		{
			*iter = nullptr;
			enemiesInRange.erase(iter);
		}

		targetedEnemy = nullptr;
		targetedEnemy = findClosestEnemy();
		setRotate(targetedEnemy == nullptr ? false : true);
	}
}

const float ADefense::calculateAngleToRotate()
{
	if (targetedEnemy != nullptr)
	{
		const FVector enemyLocation = FVector(targetedEnemy->GetActorLocation().X, targetedEnemy->GetActorLocation().Y, 0.0f);
		const FVector defenseHeadLocation = FVector(structure->findComponent("Head")->GetActorLocation().X, structure->findComponent("Head")->GetActorLocation().Y,0.0f);
		const FVector defenseToEnemyVector = enemyLocation - defenseHeadLocation;
		const FVector forwardVector = structure->findComponent("Head")->getRightVectorEnd();

		const float forwardVecMag = forwardVector.Size();
		const float defenseToEnemyVecMag = defenseToEnemyVector.Size();
		const float dotproduct = FVector::DotProduct(forwardVector, defenseToEnemyVector);
		float amount2Rotate = FMath::Acos(dotproduct / (forwardVecMag * defenseToEnemyVecMag));
		amount2Rotate = FMath::RadiansToDegrees(amount2Rotate);

		if (defenseHeadLocation.X < enemyLocation.X)
		{
			amount2Rotate *= -1;
		}
		return amount2Rotate;
	}
	return 0;
}

const float ADefense::calculateDistanceBetweenEnemyAndDefense(const FVector& enemyLoc)
{
	const FVector defenseLoc = structure->findComponent("Head")->GetActorLocation();
	const FVector defenseToEnemyVector = enemyLoc - defenseLoc;
	return defenseToEnemyVector.Size();
}

AEnemy* ADefense::findClosestEnemy()
{
	if (enemiesInRange.empty())
	{
		//shouldRotate = false;
		setRotate(false);
		return nullptr;
	}

	// this is currently the distance value we have to 'beat'
	float currentDistanceForTargetedEnemy = 
		targetedEnemy != nullptr ? calculateDistanceBetweenEnemyAndDefense(targetedEnemy->GetActorLocation()) : FLT_MAX;

	AEnemy* closestEnemy = nullptr;

	// for each enemy in range we need to work out which one is the 
	// closest to the defense and then set that as our targeted enemy
	for (const auto& param : enemiesInRange)
	{
		float distanceToEnemy = calculateDistanceBetweenEnemyAndDefense(param->GetActorLocation());
		if (distanceToEnemy <= currentDistanceForTargetedEnemy)
		{
			currentDistanceForTargetedEnemy = distanceToEnemy;
			closestEnemy = param;
		}
	}
	return closestEnemy;
}

void ADefense::fire()
{
	canFire = false;

	FActorSpawnParameters spawnActorParams;
	spawnActorParams.Owner = this;

	if (bullets.size() < MAX_BULLETS)
	{
		ABullet* bullet = GetWorld()->SpawnActor<ABullet>(FVector(0.0, 0.0, 0.0f), FRotator(0.0f, 0.0f, 0.0f), spawnActorParams);
		setupBullet(bullet);
		bullets.emplace_back(bullet);
	}
	else
	{
		bullets.emplace_back(nullptr);
		std::iter_swap(bullets.begin(), bullets.begin() + bullets.size() - 1);
		bullets.erase(bullets.begin());
		setupBullet(*bullets.begin());
	}

	GetWorldTimerManager().SetTimer(fireTimer, timerDel, stats.fireRate, true);
}

void ADefense::enableFiring()
{
	canFire = true;
	GetWorldTimerManager().ClearTimer(fireTimer);
}

void ADefense::setupBullet(ABullet* bulletToSetup)
{
	// Calcualte the bullet spawn location from the location and dir of the head and the length of the barrel
	AGameObjectComponent* head = structure->findComponent("Head");
	FVector dir = head->GetActorRightVector();
	dir.Normalize();
	FTransform headTransform = head->GetTransform();
	headTransform.AddToTranslation(dir * stats.firingOffset);
	bulletToSetup->setup(headTransform, head->getRightVectorEnd(), 5.0f, stats.ammoType);
}

void ADefense::setIsPossessed()
{
	isPossessed = true;

	AGameObjectComponent* head = structure->findComponent(FDefenseComponents::defenseHead);
	FVector pos = head->GetActorLocation();
	SetActorLocation(pos);

	FRotator rot = GetActorRotation();
	SetActorRotation(FRotator(rot.Roll, rot.Pitch + 90.0f, rot.Yaw));

	bBlockInput = false;
}

void ADefense::setNotPossessed()
{
	isPossessed = false;
	targetedEnemy = findClosestEnemy();
}

void ADefense::turn(float val)
{
	if (isPossessed)
	{
		structure->findComponent("Head")->rotateComponentRoundZAxis(val);
	}
}

void ADefense::fireClick()
{
	if (isPossessed)
	{
		enableFiring();
		fire();
	}
}

void ADefense::setRotate(bool newRotateState)
{
	if (!isPossessed)
	{
		// if this defense isn't being possessed 
		// we can set the state as intended
		shouldRotate = newRotateState;
	}
	else
	{
		// if this defene is being possessed
		// we should not allow the rotation 
		// (as the player is in control of that)
		shouldRotate = false;
	}
}

const int ADefense::getElapsedSecondsCount()
{
	return elapsedSecondCount;
}

const int ADefense::getPlayerOnBoardTimer()
{
	return playerOnBoardTimer;
}

const int ADefense::calculatePOBTimerValueForDisplay()
{
	return playerOnBoardTimer - elapsedSecondCount;
}