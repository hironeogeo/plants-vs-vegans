// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "DefenseBaseComponent.h"
#include "Math/Matrix.h"
#include "Engine/StaticMesh.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values for this component's properties
UDefenseBaseComponent::UDefenseBaseComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

UDefenseBaseComponent::UDefenseBaseComponent(const char* name, const FVector& m_position)
	: componentName(name), m_pos(m_position)
{

	m_worldMat.SetIdentity();
}

UDefenseBaseComponent::UDefenseBaseComponent(const char* name, const FVector& m_position, const FRotator& m_rotation, std::string meshPath, UDefenseBaseComponent* parent)
	:componentName(name), m_pos(m_position), m_rot(m_rotation), parentNode(parent)
{
	m_worldMat.SetIdentity();
	//LoadResources(meshPath.c_str());
}

void UDefenseBaseComponent::setup(const char* name, const FVector& m_position, const FRotator& m_rotation, std::string meshPath, UDefenseBaseComponent* parent)
{
	m_worldMat.SetIdentity();
	componentName = name;
	m_pos = m_position;
	m_rot = m_rotation;
	parentNode = parent;
	componentMesh->SetVisibility(true);

}

void UDefenseBaseComponent::setup(FString compName, FString meshPath, FVector componentOffset)
{
	UStaticMeshComponent::StaticClass();
	UStaticMesh* s = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), NULL, *meshPath));
	componentMesh->SetStaticMesh(s);

	//SetActorLocation(componentOffset);
}


// Called when the game starts
void UDefenseBaseComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDefenseBaseComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UFUNCTION(BlueprintCallable)
FString UDefenseBaseComponent::getComponentname() const
{
	return componentName;
}

UFUNCTION(BlueprintCallable)
void UDefenseBaseComponent::setMesh(UStaticMeshComponent* newMesh)
{
	componentMesh = newMesh;

}