// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "PlayerHQ.h"

APlayerHQ::APlayerHQ()
{
	playerHealth = 100;
	playerDefenseFactor = 1.0;
	SetActorEnableCollision(true);
	playerFunds = 0;
	playerCurrency = nullptr;
	playerHQALive = true;
}

// Called when the game starts or when spawned
void APlayerHQ::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerHQ::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

const int APlayerHQ::getPlayerHealth() 
{
	return playerHealth;
}

const float APlayerHQ::getPlayerDefenseFactor() const
{
	return playerDefenseFactor;
}

void APlayerHQ::applyDamage(int damageToSubtract)
{
	playerHealth -= damageToSubtract - playerDefenseFactor;
	UE_LOG(LogTemp, Error, TEXT("player hQ health = %d"), playerHealth);
	playerHealthUpdateRequired.Broadcast();
	if (playerHealth <= 0)
	{
		// broadcast (send the player has died message) to rest of the game
		UE_LOG(LogTemp, Warning, TEXT("PLAYER HQ HAS DIED"));
		//playerHasDied.Broadcast();
		playerHealth = 0;
		playerHQALive = false;
	}
}

void APlayerHQ::setMesh(UStaticMesh* newMesh)
{
	tileMesh->SetStaticMesh(newMesh);
}

void APlayerHQ::setPlayerCurrency(UPlayerCurrency* newPlayerCurrency)
{
	playerCurrency = newPlayerCurrency;
}

const int APlayerHQ::getPlayerFunds()
{
	if (playerCurrency != nullptr)
	{
		return playerCurrency->getFunds();
	}
	return 0;
}

void APlayerHQ::deductFunds(int fundsToDeduct)
{
	if (playerCurrency != nullptr)
	{
		playerCurrency->deductFunds(fundsToDeduct);
	}
}

UPlayerCurrency* APlayerHQ::getCurrencyComponent()
{
	return playerCurrency;
}

const bool APlayerHQ::isPlayerHQAlive() const
{
	return playerHQALive;
}

void APlayerHQ::broadcastGameOver()
{
	playerHasDied.Broadcast();
}

void APlayerHQ::generatePlayerHQEnemySpots()
{
	constexpr uint8 numberOfPoints = 12;
	const float angleIncrements = 360.0f / numberOfPoints;

	// radius of our circle
	const auto bounds = tileMesh->Bounds;
	double radius = tileMesh->Bounds.GetBox().GetSize().Y;

	// which will act at the origin of our circle
	const FVector actorLocation = this->GetActorLocation();

	for (int i = 0; i < numberOfPoints; ++i)
	{
		FVector location = actorLocation;

		location.X += sin(FMath::DegreesToRadians(angleIncrements * i)) * radius;
		location.Y -= cos(FMath::DegreesToRadians(angleIncrements * i)) * radius;

		slots.Add(FVector(location.X, location.Y, location.Z + 100.0f));
	}
}

FVector APlayerHQ::getRandomSlot()
{
	int32 elementID = FMath::RandRange(0, numberOfSlots - 1);

	if (slots.IsValidIndex(elementID))
	{
		return slots[elementID];
	}
	return FVector(0.0f, 0.0f, 0.0f);
}