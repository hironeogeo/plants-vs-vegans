// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "Bullet.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isAlive = false;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;
	root->SetWorldLocation(GetActorLocation());

	bulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));
	bulletMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	this->bulletMesh->SetStaticMesh(SphereMeshAsset.Object);
	bulletMesh->SetVisibility(true);

	bulletType = StrengthAndWeaknessType::NONE;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isAlive)
	{
		worldTransform.AddToTranslation((defenseDirection * speed));
		SetActorLocationAndRotation(worldTransform.GetLocation(), worldTransform.GetRotation());
	}
}

void ABullet::setup(FTransform barrelTransform, FVector forwardVector, float speedValue, StrengthAndWeaknessType type)
{
	worldTransform = barrelTransform;
	defenseDirection = forwardVector;
	defenseDirection.Normalize();
	speed = FVector(speedValue, speedValue, 0);
	isAlive = true;
	bulletType = type;
	bulletMesh->SetVisibility(true);
	SetActorEnableCollision(true);
	SetActorHiddenInGame(false);
	SetActorLocationAndRotation(worldTransform.GetLocation(), worldTransform.GetRotation());
	SetActorScale3D(FVector(0.5f, 0.5f, 0.5f));
}

StrengthAndWeaknessType ABullet::getBulletType()
{
	return bulletType;
}

void ABullet::die()
{
	isAlive = false;
	bulletMesh->SetVisibility(false);
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorLocation(FVector(0.0f, 0.0f, -500.0f));
}