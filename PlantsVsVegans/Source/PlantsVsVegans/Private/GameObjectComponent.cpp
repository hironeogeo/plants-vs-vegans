// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#include "GameObjectComponent.h"
#include "Engine/StaticMesh.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AGameObjectComponent::AGameObjectComponent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;
	componentName = "";
	meshName = "";
	componentOffset = FVector(0.0f, 0.0f, 0.0f);
	componentRotation = FRotator(0.0f, 0.0f, 0.0f);
	componentQuaternion = FQuat(0.0f, 0.0f, 0.0f, 0.0f);
	componentMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));
	parent = nullptr;
	worldTransform = worldTransform.Identity;
	localTransform = worldTransform.Identity;
}

AGameObjectComponent::~AGameObjectComponent()
{

}

void AGameObjectComponent::setup(FString compName, UStaticMesh* mesh, FVector compOffset, AGameObjectComponent* parentComp)
{
	componentName = compName;
	componentOffset = compOffset;
	//meshName = meshPath;
	parent = parentComp;

	localTransform.SetLocation(compOffset);

	UE_LOG(LogTemp, Error, TEXT("comp name = %s"), *compName);
	//UE_LOG(LogTemp, Error, TEXT("mesh path = %s"), *meshPath);

	UStaticMesh* s = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), nullptr, TEXT("/Game/Meshes/DefenseType1/DefenseType1Head.DefenseType1Head")));

	if (mesh != nullptr)
	{
		componentMesh->SetStaticMesh(mesh);
		componentMesh->SetVisibility(true);
		SetActorHiddenInGame(false);
		componentMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);
	}
	else 
	{
		UE_LOG(LogTemp, Error, TEXT("static mesh for %s is null"), *compName)
	}
}

// Called when the game starts or when spawned
void AGameObjectComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGameObjectComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGameObjectComponent::gameObjectUpdate()
{
	localTransform.SetLocation(componentOffset);
	localTransform.SetRotation(FQuat(componentRotation));
	if (parent != nullptr)
	{
		FTransform parentTransform = parent->getWorldTransform();
		FTransform::Multiply(&worldTransform, &localTransform, &parentTransform);
	}
	else
	{
		FTransform::Multiply(&worldTransform, &localTransform, &FTransform::Identity);
	}
	SetActorLocation(worldTransform.GetLocation());
	SetActorLocationAndRotation(worldTransform.GetLocation(), worldTransform.GetRotation());

	if (componentName == "Head")
	{

		FRotator rot = localTransform.GetRotation().Rotator();
		FVector forwardFromRot = UKismetMathLibrary::GetForwardVector(rot);
		FMatrix testMat = worldTransform.ToMatrixWithScale();
		FVector vectorFromMat = testMat.GetColumn(1);
		FString matString = testMat.ToString();
		//UE_LOG(LogTemp, Warning, TEXT("MatString = "),*matString);

		FVector start = GetActorLocation();
		//FVector end = FVector(start.X, start.Y + 80000.f, start.Z);


		FVector right = GetActorRightVector();
		right.Normalize();
		right *= 4000.0f;
		FVector left = GetActorRightVector();
		left.Normalize();
		left *= -4000.0f;

		FVector end = start + right;

		vectorFromMat * -1.0f;

		//DrawDebugLine(GetWorld(), start, end, FColor::Red, false, -1.0f, 0, 10.0f);
		//DrawDebugLine(GetWorld(), start, left, FColor::Red, false, -1.0f, 0, 10.0f);
	}
}

FRotator AGameObjectComponent::getComponentRotation()
{
	return componentRotation;
}

FVector	AGameObjectComponent::getComponentLocation()
{
	return componentOffset;
}

FTransform AGameObjectComponent::getWorldTransform()
{
	return worldTransform;
}

void AGameObjectComponent::updateComponentLocationWithIncrement(FVector incrementalLocationChange)
{
	componentOffset.X += incrementalLocationChange.X;
	componentOffset.Y += incrementalLocationChange.Y;
	componentOffset.Z += incrementalLocationChange.Z;

}
void AGameObjectComponent::updateComponentRotaionWithIncrement(FRotator incrementalRotationChange)
{
	componentRotation.Yaw += incrementalRotationChange.Yaw;
	componentRotation.Roll += incrementalRotationChange.Roll;
	componentRotation.Pitch += incrementalRotationChange.Pitch;
}

void AGameObjectComponent::updateComponentRotaion(FRotator newRotation)
{
	componentRotation.Yaw = newRotation.Yaw;
	componentRotation.Roll = newRotation.Roll;
	componentRotation.Pitch = newRotation.Pitch;
}

FVector AGameObjectComponent::getRightVectorFromLocation()
{
	FVector start = GetActorLocation();
	FVector right = GetActorRightVector();
	//right *= 100.0f;
	return start + right;
}

FVector AGameObjectComponent::getRightVectorEnd()
{
	FVector start = GetActorLocation();
	FVector right = GetActorRightVector();
	right *= 4000.0f;
	return start + right;
}

void AGameObjectComponent::rotateComponentRoundZAxis(int amountToRotate)
{
	componentRotation.Yaw += amountToRotate;
}