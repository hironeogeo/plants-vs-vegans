// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "PlayerCurrency.h"

// Sets default values for this component's properties
UPlayerCurrency::UPlayerCurrency()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	playerFunds = 0;

	// ...
}


// Called when the game starts
void UPlayerCurrency::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlayerCurrency::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlayerCurrency::deductFunds(const int& fundsToDeduct)
{
	if ((playerFunds - fundsToDeduct) <= 0)
	{
		playerFunds = 0;
	}
	else
	{
		playerFunds -= fundsToDeduct;
	}
	currencyHUDUpdate.Broadcast();
}

void UPlayerCurrency::addFunds(const int& fundsToAdd)
{
	playerFunds += fundsToAdd;
	currencyHUDUpdate.Broadcast();
}

void UPlayerCurrency::setFunds(const int& fundsValue)
{
	playerFunds = fundsValue;
}

const int UPlayerCurrency::getFunds()
{
	return playerFunds;
}