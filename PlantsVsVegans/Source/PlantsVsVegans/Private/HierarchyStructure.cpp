// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "HierarchyStructure.h"
#include <fstream>
#include <algorithm>
#include "Engine/World.h"
#include "defense.h"
#include <sstream>

using namespace std;


HierarchyStructure::HierarchyStructure()
{

}

HierarchyStructure::HierarchyStructure(FString name)
	:hierarchyName(name)
{
	//world = worldPtr;
	//nfilePath = DefenseTYPE1.txt
	// hierarchyName = DefenseTYPE1
}

HierarchyStructure::HierarchyStructure(FString name, UWorld* worldPtr, ADefense* defensePtr, UDefenseData* defenseDataPtr)
	:hierarchyName(name)
{
	world = worldPtr;
	defenseParent = defensePtr;
	defenseData = defenseDataPtr;
}

HierarchyStructure::~HierarchyStructure()
{
	for (auto& param : structure)
	{
		param.second = nullptr;
	}
}

// TO BE CHANGED TO PARSE DEFENSE DATA
void HierarchyStructure::readInFromFile()
{
	FString parentName = "";
	FActorSpawnParameters spawnActorParams;
	spawnActorParams.Owner = defenseParent;
	AGameObjectComponent* parentNode = nullptr;

	if (defenseData == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("defenseData is null"));
		return;
	}

	TArray<FComponentData> defenseAttributes = defenseData->getDefenseAttributes();

	if (defenseAttributes.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("defenseAttributes is empty"));
		return;
	}

	for (const auto& param : defenseAttributes)
	{
		if (param.ComponentName != "root")
		{
			parentNode = findComponent(param.Parent);
			structure[param.ComponentName] = defenseParent->GetWorld()->SpawnActor<AGameObjectComponent>(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, 0.0f), spawnActorParams);
			structure[param.ComponentName]->setup(param.ComponentName, param.Mesh, param.Offset, parentNode);
		}
		else
		{
			structure[param.ComponentName] = defenseParent->GetWorld()->SpawnActor<AGameObjectComponent>(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, 0.0f), spawnActorParams);
			structure[param.ComponentName]->setup(param.ComponentName, param.Mesh, param.Offset, nullptr);
		}
		componentOrder.push_back(param.ComponentName);
	}
}

AGameObjectComponent* HierarchyStructure::findComponent(FString criteria) // searches through the tree to find a component starting at the top
{
	return structure[criteria];
}

std::map<FString, AGameObjectComponent*> HierarchyStructure::getMap()
{
	return structure;
}

FString HierarchyStructure::getHierarchyName() const
{
	return hierarchyName;
}

std::vector<FString> HierarchyStructure::getComponentOrder() const
{
	return componentOrder;
}

TArray<AGameObjectComponent*> HierarchyStructure::getComponents()
{
	TArray<AGameObjectComponent*> components;
	components.Reserve(structure.size());

	for (const auto& param : structure)
	{
		components.Add(param.second);
	}
	return components;
}
