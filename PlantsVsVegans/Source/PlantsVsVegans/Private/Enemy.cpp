// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "Enemy.h"
#include "EnemyStart.h"
#include "PlayerHQ.h"
#include "Bullet.h"
#include "AMapManager.h"
#include "LoadBalancerTile.h"


// Sets default values
AEnemy::AEnemy()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	enemyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));
	enemyMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);

	capsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp"));
	if (capsuleComponent)
	{
		capsuleComponent->InitCapsuleSize(50, 10000);
		capsuleComponent->SetCollisionProfileName(TEXT("Trigger"));
		capsuleComponent->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);
		capsuleComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::onOverlapBegin);
		capsuleComponent->OnComponentEndOverlap.AddDynamic(this, &AEnemy::onOverlapEnd);
	}

	SetActorHiddenInGame(true);
	isAlive = false;
	shouldMove = false;
	SetActorEnableCollision(false);

	enemyStats.health = 0;
	enemyStats.defense = 1.0f;
	enemyStats.speed = 1;

	movingToNextTile = false;
	reachedNextTile = false;
}

void AEnemy::setupEnemy(AEnemyStart* enemyStartPtr, UStaticMesh* newMesh, TArray<FPair> newRoute, AMapTile* enemyStartTilePtr)
{
	setManager(enemyStartPtr);
	setMesh(newMesh);
	setRoute(newRoute);
	currentTile = enemyStartTilePtr;
	findNextTargetTile();
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isAlive && shouldMove)
	{
		if (gotPlayerHQSlot)
		{
			moveToPlayerHQ();
		}
		else
		{
			moveAlongAssignedPath();
		}
	}
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

const bool AEnemy::getIsAlive() const
{
	return isAlive;
}

void AEnemy::setIsAlive(bool newIsAliveState)
{
	isAlive = newIsAliveState;
}

void AEnemy::die()
{
	/* Once an enemies health has reached 0 then that enemy needs to die.

	// this means that we need to:
	- change the enemies isAlive status
	- stop it from being rendered / make it invisible
	- turn of it's collisions
	- and let manager know that an enemy has died so it can be reset if needs be

	*/

	isAlive = false;
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorLocation(FVector(0.0f, 0.0f, 500.0f));
	manager->reportEnemyDeath(this);
	gotPlayerHQSlot = false;

	// stop attacking the player as the enemy is no longer in contact with the player
	GetWorldTimerManager().ClearTimer(attackTimer);
}

void AEnemy::attack(APlayerHQ* playerHQ)
{
	UE_LOG(LogTemp, Warning, TEXT("Attacking the player"));

	// if both the enemy and the player HQ is alive
	if (isAlive && playerHQ->isPlayerHQAlive())
	{
		if (playerHQ->getPlayerHealth() > 0)
		{
			playerHQ->applyDamage(enemyStats.damageCanInflict);
		}
	}
}

AEnemyStart* AEnemy::getManager()
{
	return manager;
}

void AEnemy::damageEnemy(const int& damageToEnemy)
{
	enemyStats.health -= damageToEnemy;

	if (enemyStats.health <= 0)
		die();
}
void AEnemy::setDefense(const float& newEnemyDefense)
{
	enemyStats.defense = newEnemyDefense;
}

void AEnemy::setSpeed(const int& newEnemySpeed)
{
	enemyStats.speed = newEnemySpeed;
}

void AEnemy::setEnemyType(EnemyTypes newEnemyType)
{
	enemyType = newEnemyType;
}

void AEnemy::setEnemySubType(EnemySubTypes newEnemySubType)
{
	enemySubType = newEnemySubType;
}

void AEnemy::setupEnemyStatusPack(FEnemyStatsPack newEnemyStatsPack)
{
	enemyStats = newEnemyStatsPack;
}

void AEnemy::setMesh(UStaticMesh* newStaticMesh)
{
	enemyMesh->SetStaticMesh(newStaticMesh);
}

void AEnemy::onOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isAlive)
	{
		if (OtherActor && (OtherActor != this) && OtherComp)
		{
			// if the actor we have collided with is a PlayerHQ
			if (OtherActor->IsA<APlayerHQ>())
			{
				handlePlayHQCollision(OtherActor);
			}
			else if (OtherActor->IsA<ABullet>())
			{
				handleBulletCollision(OtherActor);
			}
		}
	}
}

void AEnemy::onOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		FString otherActorName = OtherActor->GetName();
		FString humanReadableName = OtherActor->GetHumanReadableName();
	}
}

const bool AEnemy::getShouldMove() const
{
	return shouldMove;
}

void AEnemy::setShouldMove(bool newShouldMove)
{
	shouldMove = newShouldMove;
}

void AEnemy::handlePlayHQCollision(AActor* OtherActor)
{
	// we need to make a playerHQ reference
	APlayerHQ* playerHQ = Cast<APlayerHQ, AActor>(OtherActor);

	// we need to get the playerHQ to allocate us a slot to attack from
	playerHQSlotLocation = playerHQ->getRandomSlot();
	playerHQLocation = playerHQ->GetActorLocation();
	gotPlayerHQSlot = true;
	shouldMove = true;

	// bind the player parameter that we need for the attck function 
	timerDel.BindUFunction(this, FName("attack"), playerHQ);

	// set the timer to attack for every attack rate seconds
	GetWorldTimerManager().SetTimer(attackTimer, timerDel, enemyStats.attackRate, true);
}

void AEnemy::handleBulletCollision(class AActor* OtherActor)
{
	ABullet* bullet = Cast<ABullet, AActor>(OtherActor);
	StrengthAndWeaknessType bulletType = bullet->getBulletType();
	Effectiveness effectiveness = UStrengthsAndWeaknesses::resolveBulletEnemyCollision(enemyStats.strongAgainst, enemyStats.weakAgainst, bulletType);
	bullet->die();
	switch (effectiveness)
	{
	case Effectiveness::STRONG:
		damageEnemy(3);
		break;
	case Effectiveness::NORMAL:
		damageEnemy(2);
		break;
	case Effectiveness::WEAK:
		damageEnemy(1);
		break;
	default:
		break;
	}
}

void AEnemy::setManager(AEnemyStart* managerPtr)
{
	manager = managerPtr;
}

void AEnemy::setRoute(TArray<FPair> newRoute)
{
	route = newRoute;
}

void AEnemy::setCurrentTile(AMapTile* newCurrentTile)
{
	currentTile = newCurrentTile;
}

AMapTile* AEnemy::findNextTargetTile()
{
// we need to calculate the movement vector for the
// current tile that we are on to the next tile

// to start with the enemy start will tell us what cell we are on

// then we can find the next cell coordinates by looking at the start of the route array
// - remember to remove the pair we've just got from the route array
	FPair nextTileCoordinates = route[0];
	route.RemoveAt(0);

	// we now need to turn the pair coordinates into a cell so we need to do a lookup in the map manager
	int nextTileID = getManager()->getManager()->twodToOneDGridConversion(nextTileCoordinates);
	targetTile = getManager()->getManager()->mapTilesGrid[nextTileID];
	return targetTile;
}

bool AEnemy::hasReachedNextTile()
{
	FVector adjustedActorLocation = generateGridAdjustedVector(GetActorLocation());
	FVector adjustedTargetLocation = generateGridAdjustedVector(targetTile->GetActorLocation());
	FVector vecToTarget = adjustedTargetLocation - adjustedActorLocation;

	double distanceToTarget = vecToTarget.Size();

	if (distanceToTarget <= distanceErrorMargin)
	{
		SetActorLocation(adjustedTargetLocation);
		return true;
	}
	return false;
}

FVector AEnemy::generateGridAdjustedVector(const FVector& inputVector)
{
	FVector temp(inputVector);
	temp.Z = 100;
	return temp;
}

bool AEnemy::hasReachedEndOfPath()
{
	return route.Num() <= 0;
}

void AEnemy::setTargetTile(AMapTile* newTargetTile)
{
	targetTile = newTargetTile;
}

FVector AEnemy::calculateMovementVector()
{
	// now we calculate how to get from where we are to the target tile
	vectorToNextTile = targetTile->GetActorLocation() - currentTile->GetActorLocation();
	return vectorToNextTile;
}

bool AEnemy::hasReachedPlayerHQSlot()
{
	FVector enemyToSlotVec = GetActorLocation() - playerHQSlotLocation;
	double vecMagnitude = enemyToSlotVec.Size();

	if (vecMagnitude <= distanceErrorMargin)
	{
		// clear out the route
		route = {};
		SetActorLocation(playerHQSlotLocation);
		return true;
	}
	return false;
}

// TODO: make a generic function that can be used both here and the defense rather than duplicated code
const float AEnemy::calculateAngleToRotate()
{
	const FVector playerHQLoc = FVector(playerHQLocation.X, playerHQLocation.Y, 0.0f);
	const FVector EnemyLoc = FVector(GetActorLocation().X, GetActorLocation().Y, 0.0f);
	const FVector EnemyToPlayerHQVector = playerHQLocation - EnemyLoc;
	const FVector EnemyforwardVector = GetActorForwardVector() * 4000;

	const float forwardVecMag = EnemyforwardVector.Size();
	const float EnemyToPlayerHQVecMag = EnemyToPlayerHQVector.Size();
	const float dotproduct = FVector::DotProduct(EnemyforwardVector, EnemyToPlayerHQVector);

	float amount2Rotate = FMath::Acos(dotproduct / (forwardVecMag * EnemyToPlayerHQVecMag));
	amount2Rotate = FMath::RadiansToDegrees(amount2Rotate);

	if (EnemyLoc.X > playerHQLocation.X)
	{
		amount2Rotate *= -1;
	}
	return amount2Rotate;
}

void AEnemy::moveToPlayerHQ()
{
	// if the enemy hasn't reached the slot they've been assigned
	// incremently move them towards it
	if (!hasReachedPlayerHQSlot())
	{
		vectorToPlayerHQSlot = playerHQSlotLocation - GetActorLocation();
		vectorToPlayerHQSlot.Normalize();
		vectorToPlayerHQSlot *= enemyStats.speed;

		SetActorLocation(GetActorLocation() += vectorToPlayerHQSlot);
		return;
	}

	// once the enemy has reached the slot
	// rotate them to face the playerHQ
	const double amount2Rotate = calculateAngleToRotate();
	auto actorRot = GetActorRotation();
	actorRot.Yaw += amount2Rotate;

	SetActorRotation(FQuat(actorRot));
	rotatedToFacePlayerHQ = true;
}

void AEnemy::moveAlongAssignedPath()
{
	if (!hasReachedEndOfPath())
	{
		// we've not reached the next tile so we should continue incrementally moving towards it
		if (!hasReachedNextTile())
		{
			movingToNextTile = true;
			vectorToNextTile = generateGridAdjustedVector(targetTile->GetActorLocation()) - generateGridAdjustedVector(GetActorLocation());
			vectorToNextTile.Normalize();
			vectorToNextTile *= enemyStats.speed;

			SetActorLocation(GetActorLocation() += vectorToNextTile);
			return;
		}

		// if we've reached the target tile we should check if it's a load balancer tile
		// and then request our new route
		if (targetTile->getIsLoadBalancer())
		{
			ALoadBalancerTile* lbTile = (ALoadBalancerTile*)targetTile;
			route = lbTile->generateEnemyRouteRandom();
		}

		movingToNextTile = false;
		SetActorLocation(targetTile->getAdjustedLocation());
		findNextTargetTile();
	}
}
