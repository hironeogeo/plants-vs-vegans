﻿// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "PathFinder.h"

//template<typename T, size_t COL, size_t ROW>
/*/*inline*/ void PathFinder/*<T, COL, ROW>*/::popOpenList()
{
	openList.pop();
}

//template<typename T, size_t COL, size_t ROW>
/*/*inline*/ void PathFinder/*<T, COL, ROW>*/::removeVertexFromClosedList(int a, int b)
{
	newClosedList[a][b] = true;
}

//template<typename T, size_t COL, size_t ROW>
/*/*inline*/ void PathFinder/*<T, COL, ROW>*/::setup(const Pair& start, const Pair& destination)
{
	lastDest = destination;
	destinationFound = false;

	// PathFinder.src and PathFinder.dest must have been assigned for this function to be called
	newClosedList = std::vector<std::vector<bool>>(ROW, std::vector<bool>(COL, false));
	newCellDetails = std::vector<std::vector<cell>>(ROW, std::vector<cell>(COL, cell()));

	// Initialising the parameters of the starting node
	i = start.first, j = start.second;
	newCellDetails[i][j].f = 0.0;
	newCellDetails[i][j].g = 0.0;
	newCellDetails[i][j].h = 0.0;
	newCellDetails[i][j].parent = { i, j };

	/* Create an open list having information as-
	<f, <i, j>>
	where f = g + h,
	and i, j are the ROW and COLumn index of that cell
	Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
	This open list is implemented as a set of tuple.*/
	//std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;

	// Put the starting cell on the open list and set its
	// 'f' as 0

	openList.emplace(0.0, i, j);
}

/*inline*/ PathFinder::PathFinder()
{
	// north , east, south, west
// north east, north west, south east, south west
	directions = {
		Pair(0,1), Pair(1,0), Pair(0,-1), Pair(-1,0)
		//,Pair(1,1), Pair(-1,1), Pair(1,-1), Pair(-1,-1)
	};
}

//template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest)
//{
//	//memset(newClosedList, false, sizeof(newClosedList));
//	this->src = src;
//	this->dest = dest;
//
//	assert(verifyClassIntegrity(newGrid, src, dest));
//
//}

//template<typename T, size_t COL, size_t ROW>
///*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(const Pair& src, const Pair& dest, array<array<T, COL>, ROW> grid)
//{
//	//memset(newClosedList, false, sizeof(newClosedList));
//	this->src = src;
//	this->dest = dest;
//	this->newGrid = grid;
//
//	assert(verifyClassIntegrity(newGrid, src, dest));
//}



//template<typename T, size_t COL, size_t ROW>
/*inline*/ PathFinder/*<T, COL, ROW>*/::PathFinder(std::vector<std::vector<char>> grid, int row, int col)
{
	this->ROW = row;
	this->COL = col;
	this->newGrid = grid;

	directions = {
	Pair(0,1), Pair(1,0), Pair(0,-1), Pair(-1,0)
	//,Pair(1,1), Pair(-1,1), Pair(1,-1), Pair(-1,-1)
	};

	assert(verifyClassIntegrity(newGrid, src, dest));
}

//template<typename T, size_t COL, size_t ROW>

//template<typename T, size_t COL, size_t ROW>
/*inline*/ PathFinder/*<T, COL, ROW>*/::~PathFinder()
{

}

void PathFinder/*<T, COL, ROW>*/::aStarSearch(const Pair& start, const Pair& destination)
{
	setup(start, destination);

	// We set this boolean value as false as initially
	// the destination is not reached.
	while (!openList.empty())
	{
		const Tuple& p = openList.top();
		// Add this vertex to the closed list
		i = get<1>(p); // second element of tupla
		j = get<2>(p); // third element of tupla

		// Remove this vertex from the open list
		openList.pop();
		newClosedList[i][j] = true;
		/*
				Generating all the 8 successor of this cell
						N.W N N.E
						\ | /
						\ | /
						W----Cell----E
								/ | \
						/ | \
						S.W S S.E

				Cell-->Popped Cell (i, j)
				N --> North	 (i-1, j)
				S --> South	 (i+1, j)
				E --> East	 (i, j+1)
				W --> West		 (i, j-1)
				N.E--> North-East (i-1, j+1)
				N.W--> North-West (i-1, j-1)
				S.E--> South-East (i+1, j+1)
				S.W--> South-West (i+1, j-1)
		*/
		//for (int add_x = -1; add_x <= 1; add_x++) {
			//for (int add_y = -1; add_y <= 1; add_y++) {
		Pair currentPos = Pair(i, j);
		for (Pair neighbour : getNeighbors(currentPos)) {
				//Pair neighbour(i + add_x, j + add_y);
				// Only process this cell if this is a valid
				// one
				if (isValid(newGrid, neighbour, ROW, COL)) {
					// If the destination cell is the same
					// as the current successor
					if (isDestination(neighbour, destination))
					{ // Set the Parent of the destination cell
						newCellDetails[neighbour.first][neighbour.second].parent = { i, j };
						printf("The destination cell is found\n");
						//tracePath(newCellDetails, dest);
						destinationFound = true;
						return;
					}
					// If the successor is already on the
					// closed list or if it is blocked, then
					// ignore it. Else do the following
					else if (!newClosedList[neighbour.first][neighbour.second]
						&& isUnBlocked(newGrid, neighbour, ROW, COL))
					{
						double gNew, hNew, fNew;
						gNew = newCellDetails[i][j].g + 1.0; // TODO: GEORGE THIS IS WHERE WE NEED TO IMPLEMENT THE COST CALCULATION
						hNew = calculateManhattenHVal(neighbour, destination); //calculateHValue(neighbour, destination);
						fNew = gNew + hNew;

						// If it isn�t on the open list, add
						// it to the open list. Make the
						// current square the parent of this
						// square. Record the f, g, and h
						// costs of the square cell
						//			 OR
						// If it is on the open list
						// already, check to see if this
						// path to that square is better,
						// using 'f' cost as the measure.
						if (newCellDetails[neighbour.first][neighbour.second].f == -1
							|| newCellDetails[neighbour.first][neighbour.second].f > fNew)
						{
							openList.emplace(fNew, neighbour.first, neighbour.second);

							// Update the details of this
							// cell
							newCellDetails[neighbour.first][neighbour.second].g = gNew;
							newCellDetails[neighbour.first][neighbour.second].h = hNew;
							newCellDetails[neighbour.first][neighbour.second].f = fNew;
							newCellDetails[neighbour.first][neighbour.second].parent = { i, j };
						}
					}
				}
			}
		//}
	}

	// When the destination cell is not found and the open
	// list is empty, then we conclude that we failed to
	// reach the destiantion cell. This may happen when the
	// there is no way to destination cell (due to
	// blockages)
	printf("Failed to find the Destination Cell\n");
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const Pair PathFinder/*<T, COL, ROW>*/::getCurrentCell() const
{
	return currentCell;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const Pair PathFinder/*<T, COL, ROW>*/::getCellInvestigating() const
{
	return cellInvestigating;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const std::vector<Pair> PathFinder/*<T, COL, ROW>*/::getCellsInvestigating() const
{
	return cellsInvestigating;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ auto PathFinder/*<T, COL, ROW>*/::getGrid() const
{
	return newGrid;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getClosedList() const
{
	return newClosedList;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getCellDetails() const
{
	return newCellDetails;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const auto PathFinder/*<T, COL, ROW>*/::getOpenList() const
{
	return openList;
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const bool PathFinder/*<T, COL, ROW>*/::isOpenListEmpty() const
{
	return openList.empty();
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ void PathFinder/*<T, COL, ROW>*/::getVertexAndUpdateLists()
{
	//const Tuple& p = getOpenList().top();
	const Tuple& p = openList.top();

	// Add this vertex to the closed list
	i = get<1>(p); // second element of tupla
	j = get<2>(p); // third element of tupla

	// Remove this vertex from the open list
	popOpenList();
	removeVertexFromClosedList(i, j);
}

//template<typename T, size_t COL, size_t ROW>
/*inline*/ const bool PathFinder/*<T, COL, ROW>*/::isDestinationFound() const
{
	return destinationFound;
}

// A Utility Function to trace the path from the source to
// destination

void PathFinder::tracePathForGame(std::stack<Pair>& route)
{
	printf("\nThe Path is ");

	//std::stack<Pair> path;
	route.push(lastDest);

	int row = lastDest.first;
	int col = lastDest.second;
	Pair next_node;
	do {
		next_node = newCellDetails[row][col].parent;
		row = next_node.first;
		col = next_node.second;
		route.push(next_node);
	} while (newCellDetails[row][col].parent != next_node);
}

std::vector<Pair> PathFinder::getNeighbors(Pair currentCellPos)
{
	std::vector<Pair> neighbors;
	neighbors.reserve(4);

	for (Pair dir : directions)
	{
		Pair next(currentCellPos.first + dir.first, currentCellPos.second + dir.second);
		if (isValid(newGrid, next, ROW, COL)) {
			neighbors.push_back(next);
		}
	}
	return neighbors;
}

std::vector<Pair> PathFinder::getLoadBalancerNeighbors(Pair currentCellPos)
{
	std::vector<Pair> neighbors;
	neighbors.reserve(4);

	for (Pair dir : directions)
	{
		Pair next(currentCellPos.first + dir.first, currentCellPos.second + dir.second);
		if (isValid(newGrid, next, ROW, COL) 
			&& newGrid[next.first][next.second] == 'p') {
			neighbors.push_back(next);
		}
	}
	return neighbors;
}
