// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#include "AMapManager.h"
#include "Kismet/KismetStringLibrary.h"
#include "EnemyStart.h"
//#include <vector>
#include "PathFinder.h"
#include "LoadBalancerTile.h"
#include <iterator>

// Sets default values
AAMapManager::AAMapManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mapString = "";
	heightCoutner = 0;

}

// Sets default values
AAMapManager::~AAMapManager()
{
	delete pathfinder;

}

// Called when the game starts or when spawned
void AAMapManager::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AAMapManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAMapManager::buildLevelFromMapFile()
{
	if (MapData == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("MapData is nullptr"));
		return;
	}

	if (!MapData->IsLevelDataValid())
	{
		UE_LOG(LogTemp, Error, TEXT("MapData is not valid"));
		return;
	}
	
	mapHeight = MapData->MapHeight;
	mapWidth = MapData->MapWidth;

	parseMapFile(MapData->MapString);

	// now we have parsed the map file 
	// we need to make sure we can trace a path
	// from each enemy start and load balancer to the player HQ

	pathfinder = new PathFinder(mapGrid, MapData->MapHeight, MapData->MapWidth);

	testCombinations(createCombinations());

	// check to make sure we have at least 1 route in the game
	if (routes.empty())
	{
		UE_LOG(LogTemp, Error, TEXT("ERROR no paths not found"));
	}

	// check to make sure we have at least 1 route for each start location
	for (const auto& param : startLocations)
	{
		auto tmpRoutes = getRoutesByCellLocation(param);
		
		if (tmpRoutes.empty())
		{
			UE_LOG(LogTemp, Error, TEXT("ERROR no path not found for start location at location (%d,%d)"), param.first, param.second);

		}
	}

	if (!loadBalancerLocations.empty())
	{
		// check to make sure we have at least 1 route for each load balancer tile location
		for (const auto& param : loadBalancerLocations)
		{
			auto tmpRoutes = getRoutesByCellLocation(param);

			if (tmpRoutes.empty())
			{
				UE_LOG(LogTemp, Error, 
					TEXT("ERROR no path not found for load balancer location at location (%d,%d)"), 
					param.first, param.second);
			}
		}
	}	
}

void AAMapManager::parseMapFile(FString mapFileString)
{
	if (MapData == nullptr)
	{
		return;
	}

	auto mapChars = mapFileString.GetCharArray();
	mapChars.Remove('\r');
	mapChars.Remove('\n');
	mapChars.Remove('\0');
	mapChars.Remove('1');

	auto iter = mapChars.CreateIterator();

	auto iterEnd = mapChars.CreateIterator();
	iterEnd.SetToEnd();

	mapGrid = std::vector<std::vector<char>> (MapData->MapHeight, std::vector<char>(MapData->MapWidth, 'U'));
	int counter = 0;
	
	for (iter.Reset(); iter != iterEnd; ++iter)
	{	
		if (counter >= MapData->MapWidth)
		{
			heightCoutner++;
			counter = 0;
		}

		switch (*iter)
		{
		case 'x':
			mapEnum.Add(MapTileCategories::STANDARD);
		break;

		case 'E':
			startLocations.push_back(make_pair(heightCoutner, counter));
			mapEnum.Add(MapTileCategories::ENEMYSTART);
		break;

		case 'H':
			destinationLocations.push_back(make_pair(heightCoutner, counter));
			mapEnum.Add(MapTileCategories::PLAYERSTART);
		break;

		case 'p':
			mapEnum.Add(MapTileCategories::PATH);
		break;

		case 'L':
			loadBalancerLocations.push_back(make_pair(heightCoutner, counter));
			mapEnum.Add(MapTileCategories::LOADBALANCER);

		default:
			break;
		}

		mapGrid[heightCoutner][counter] = *iter;
		counter++;
	}
}

int AAMapManager::getMapHeight()
{
	return mapHeight;
}

int AAMapManager::getMapWidth()
{
	return mapWidth;
}

const MapTileCategories AAMapManager::generateMapTileEnum(TCHAR mapStringParam) 
{
	MapTileCategories enumToReturn;

	if (mapStringParam == 'x')
		enumToReturn = MapTileCategories::STANDARD;
	else if (mapStringParam == 'E')
		enumToReturn = MapTileCategories::ENEMYSTART;
	else if (mapStringParam == 'H')
		enumToReturn = MapTileCategories::PLAYERSTART;
	else if (mapStringParam == 'p')
		enumToReturn = MapTileCategories::PATH;
	else if (mapStringParam == 'L')
		enumToReturn = MapTileCategories::LOADBALANCER;
	else
		enumToReturn = MapTileCategories::UNKNOWN;
	
	if (enumToReturn != MapTileCategories::UNKNOWN)
		mapString.AppendChar(mapStringParam);
	return enumToReturn;
}

TArray<MapTileCategories> AAMapManager::getMapEnumArray()
{
	return mapEnum;
}

TArray<FPair> AAMapManager::generateEnemyRoute()
{
	TArray<FPair> route;
	std::stack<std::pair<int, int>> routeCopy = enemyRoute;
	
	while (!routeCopy.empty()) {
		Pair p = routeCopy.top();
		routeCopy.pop();
		route.Add(FPair(p.first, p.second));
	}
	return route;
}

TArray<FPair> AAMapManager::generateEnemyRoute(std::stack<Pair> enemyRouteParam)
{
	TArray<FPair> route;
	std::stack<Pair> routeCopy = enemyRouteParam;

	while (!routeCopy.empty()) {
		Pair p = routeCopy.top();
		routeCopy.pop();
		route.Add(FPair(p.first, p.second));
	}
	return route;
}

void AAMapManager::addMapTilesGridElement(AMapTile* mapTile)
{
	mapTilesGrid.Add(mapTile);
}

int AAMapManager::twodToOneDGridConversion(FPair twoDGridDimmention)
{
	// x+(x_size*Y)
	return twoDGridDimmention.y + (mapWidth * twoDGridDimmention.x);
}

FPair AAMapManager::onedToTwoDGridConversionFPair(int counter)
{
	// pair.x = counter / height
	// pair.y = counter % width

	FPair pair;
	pair.x = counter / mapHeight;
	pair.y = counter % mapHeight;
	return pair;
}

Pair AAMapManager::onedToTwoDGridConversionPair(int counter)
{
	// pair.x = counter / height
	// pair.y = counter % width

	Pair pair;
	pair.first = counter / mapHeight;
	pair.second = counter % mapHeight;
	return pair;
}

std::vector<ProcessingUnit*> AAMapManager::createCombinations()
{
	// container to store the combinations to be tested later on
	std::vector<ProcessingUnit*> combinationsToProcess;

	// container to store the intermediate pairs for the load balancing neighbores to be tested
	std::vector<InterProcessingUnit*> interPairs;

	// outer loop for each start location
	for (int i = 0; i < startLocations.size(); ++i)
	{
		// inner loop for the destinations
		for (int j = 0; j < destinationLocations.size(); ++j)
		{
			combinationsToProcess.push_back(new ProcessingUnit(startLocations[i], destinationLocations[j]));
		}
	}

	// outer loop for load balancer locations
	for (int i = 0; i < loadBalancerLocations.size(); ++i)
	{
		std::vector<Pair> neighbores;
		neighbores.reserve(4);
		neighbores = pathfinder->getLoadBalancerNeighbors(loadBalancerLocations[i]);

		// inner loop to create inter processing units from the valid neighbores of each load balancer location
		for (int j = 0; j < neighbores.size(); ++j)
		{
			interPairs.push_back(new InterProcessingUnit(loadBalancerLocations[i], neighbores[j]));
		}
	}

	// outer loop for each inter processing unit
	for (int i = 0; i < interPairs.size(); ++i)
	{
		// innner loop to create a combination to process for each load balancer neighbor and destination
		for (int j = 0; j < destinationLocations.size(); ++j)
		{
			combinationsToProcess.push_back(new ProcessingUnit(interPairs[i]->parent, interPairs[i]->neighborCell, destinationLocations[j]));
		}

		delete interPairs[i];
		interPairs[i] = nullptr;
	}
	return combinationsToProcess;
}

void AAMapManager::testCombinations(std::vector<ProcessingUnit*> combinations)
{
	// for each combination we need to test if their is a valid path
	for (auto& param : combinations)
	{
		pathfinder->aStarSearch(param->start, param->dest);
		if (pathfinder->isDestinationFound())
		{
			std::stack<Pair> route;
			pathfinder->tracePathForGame(route);

			// if it's a load balancing tile we need to add the the parent location into the route
			if (isParentSet(param))
			{
				route.push(param->parent);
				if (checkRouteValid(route))
				{
					route.pop();
					routes[param->parent].push_back(route);
				}
				
			}
			else
			{
				routes[param->start].push_back(route);
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Did not find a path"));
		}

		// delete the combination as we no longer need it
		delete param;
		param = nullptr;
	}
}

std::vector<std::stack<Pair>> AAMapManager::getRoutesByCellLocation(Pair cellLocation)
{
	return routes[cellLocation];
}

bool AAMapManager::isParentSet(ProcessingUnit* processingUnit)
{
	return processingUnit->parent != Pair(-1, -1);
}

void AAMapManager::getEnemyRoutes(AEnemyStart* enemyStart)
{
	std::vector<std::stack<Pair>> enemyRoutesTmp;
	//TArray<TArray<FPair>> enemyRoutes;

	enemyRoutesTmp = getRoutesByCellLocation(enemyStart->getTilePosition().toPair());

	// loop to convert the routes into a TArray<TArray<FPair>>
	//for (const auto& param : enemyRoutesTmp)
	//{
	//	enemyRoutes.Add(generateEnemyRoute(param));
	//}

	enemyStart->setEnemyRoutes(enemyRoutesTmp);

}

void AAMapManager::getLoadBalancerRoutes(ALoadBalancerTile* loadBalancer)
{
	std::vector<std::stack<Pair>> loadBalancerRoutesTmp;
	loadBalancerRoutesTmp = getRoutesByCellLocation(loadBalancer->getTilePosition().toPair());
	loadBalancer->setLoadBalanerRoutes(loadBalancerRoutesTmp);
	loadBalancer->setupWeightedRoutes();
	loadBalancer->updatePreferredRoute();
}

bool AAMapManager::checkRouteValid(std::stack<Pair> routeToCheck)
{
	std::vector<Pair> tmpRouteVec;
	tmpRouteVec.reserve(routeToCheck.size());

	// copy the elements out of the stack and into a vector 
	// so we can traverse it more easily
	while (!routeToCheck.empty()) {
		Pair p = routeToCheck.top();
		routeToCheck.pop();
		tmpRouteVec.push_back(Pair(p.first, p.second));
	}
	
	// sort the Pairs by their first element
	std::sort(tmpRouteVec.begin(), tmpRouteVec.end());

	// check for adjecent duplicate elements
	const auto tmpIter = std::adjacent_find(tmpRouteVec.begin(), tmpRouteVec.end());

	return (tmpIter == tmpRouteVec.end()) ? true : false;
	
}
