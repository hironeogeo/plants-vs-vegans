// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "Component.h"

Component::Component()
{
}

Component::Component(const char* name, const FVector& m_position)
	:componentName(name), m_pos(m_position)
{
	m_worldMat.SetIdentity();
}

Component::Component(const char* name, const FVector& m_position, const FRotator& m_rotation, std::string meshPath, Component* parent)
	:componentName(name), m_pos(m_position), m_rot(m_rotation), parentNode(parent)
{
	m_worldMat.SetIdentity();
}

Component::~Component()
{
}

FString Component::getComponentname() const
{
	return componentName;
}