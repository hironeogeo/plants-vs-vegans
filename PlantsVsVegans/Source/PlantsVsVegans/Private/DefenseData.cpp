// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020

#include "DefenseData.h"

TArray<FComponentData> UDefenseData::getDefenseAttributes() const
{
	return DefenseAttributes;
}