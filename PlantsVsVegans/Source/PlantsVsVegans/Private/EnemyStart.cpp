// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "EnemyStart.h"

AEnemyStart::AEnemyStart()
{
	// These are the default values but these can be overridden by 
	// setEnemyStartStats() which is called from the map manager
	maxNunberOfEnemies = 10;
	maxAlive = 10;

	numEnemiesSpawned = 0;
	numAliveEnemies = 0;

	durationBetweenEnemySpawn = 3.0f;
	spawnReducer = 0.2f;
	statIncreaser = 0.1f;

	// This needs to be +1 becuase when we come to recycling enemies we will need to add en empty element to enable the enemies to be reordered
	// so better to reseve the extra capacity now so we don't have to reallocate a populated container later.
	enemies.reserve(maxNunberOfEnemies + 1); 

	spawnCapacityAvaliable = true;
	stdEnemyCapacityRemaining = true;
	playerCurrency = nullptr;
}

// Called when the game starts or when spawned
void AEnemyStart::BeginPlay()
{
	Super::BeginPlay();

	FActorSpawnParameters spawnParams = FActorSpawnParameters();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	FVector vec = { 100.0f, 100.0f, 100.0f };
	FRotator rot = { 0.0f,0.0f,0.0f };

	AEnemy* spawned = GetWorld()->SpawnActor<AEnemy>(AEnemy::StaticClass(), vec, rot, spawnParams);

	//AEnemy* spawned;
	for (int i = 0; i < maxNunberOfEnemies; ++i)
	{
		 spawned = GetWorld()->SpawnActor<AEnemy>(AEnemy::StaticClass(), vec, rot, spawnParams);
		 enemies.push_back(spawned);
	}
}

// Called every frame
void AEnemyStart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyStart::recycleEnemy(AEnemy* enemyToRecycle)
{
	/*
		I think the requirements of this function has now changed because of the spawning system 
		in place in the EnemyStart_BP. Currently I think what might be really useful 
		(and may be a useful optimisation) is if we take the dead enemy and move it to the front 
		of the vector so that is will be much faster to respawn it when required

		- we should only do this though when there is a need for more enemies to be generated
		  otherwise it's a waste of processing power
	*/

	// find the enemy we want to recycle and create a iterator to it
	auto targetElementIterator = std::find(enemies.begin(), enemies.end(), enemyToRecycle); 
	if (targetElementIterator != enemies.end())
		std::iter_swap(enemies.begin(), targetElementIterator);
}

void AEnemyStart::reportEnemyDeath(AEnemy* enemy)
{
	/* this function is called when an enemy has died.
	
		when this happens the following needs to happen

		if the maxNunberOfEnemies total has not yet been reached 
		then we need to recycle the dead enemy so that it can be used as a new enemy

		otherwise we will have reached out limit and no more enemies will be spawned for the duration of the game
	*/

	UE_LOG(LogTemp, Warning, TEXT("Enemy Death Reported"));

	enemyHasDied.Broadcast();

	if (playerCurrency != nullptr)
	{
		playerCurrency->addFunds(enemy->enemyStats.currencyValue);
	}

	numAliveEnemies -= 1;

	if (numEnemiesSpawned < maxNunberOfEnemies)
	{
		recycleEnemy(enemy);
	}
	else
	{
		if (maxNunberOfEnemies >= numEnemiesSpawned && numAliveEnemies == 0)
		{
			// broadcast (send the player has won message) to rest of the game
			UE_LOG(LogTemp, Warning, TEXT("PLAYER HAS WON"));
			playerHasWon.Broadcast();
		}
	}
}

const float AEnemyStart::getDurationBetweenEnemySpawn()
{
	return durationBetweenEnemySpawn;
}

const bool AEnemyStart::getStandardEnemyCapacityRemaining()
{
	return numEnemiesSpawned < (maxNunberOfEnemies - 1);
}

AEnemy* AEnemyStart::getNewEnemyCoreForSpawn()
{
	return findAvaliableEnemy();
}

AEnemy* AEnemyStart::findAvaliableEnemy()
{
	for (const auto& param : enemies)
	{
		// Find an enemy that's dead
		if (!param->getIsAlive())
		{
			numAliveEnemies += 1;
			numEnemiesSpawned += 1;
			return param;
		}
	}
	UE_LOG(LogTemp, Error, TEXT("Called findAvaliableEnemy when there are no dead enemies"));
	return nullptr;
}

const bool AEnemyStart::getSpawnCapacityAvaliable()
{
	return numEnemiesSpawned < maxNunberOfEnemies;
}

const int AEnemyStart::getNumberOfEnemiesLeftToSpawn()
{
	return maxNunberOfEnemies - numEnemiesSpawned;
}

const bool AEnemyStart::getReachedMaxEnemiesAlive()
{
	return numAliveEnemies < maxAlive;
}

void AEnemyStart::reduceEnemySpawningTime()
{
	if (durationBetweenEnemySpawn >= minEnemySpawnTIme)
	{
		durationBetweenEnemySpawn -= spawnReducer;
		spawnReducer += spawnReducerIncrements;
	}
}

AEnemy* AEnemyStart::generateEnemy(EnemyTypes typeOfEnemy)
{
	AEnemy* newEnemy = findAvaliableEnemy();

	if (newEnemy != nullptr)
	{
		newEnemy->setIsAlive(true);
		newEnemy->SetActorHiddenInGame(false);
		newEnemy->enemyType = typeOfEnemy;
		newEnemy->enemySubType = typeOfEnemy == EnemyTypes::ENEMYGRUNT ? generateGruntSubtype() : generateBossSubtype();
		newEnemy->enemyStats = generateEnemyStats(newEnemy->enemySubType);
		newEnemy->SetActorEnableCollision(true);
		newEnemy->setShouldMove(true);
		newEnemy->setRoute(enemyRoute);
		inflateEnemyStats(newEnemy->enemyStats);

		FRotator startingRot = newEnemy->GetActorRotation();
		float ranFloatVal = FMath::RandRange(0, 350);
		startingRot.Yaw += ranFloatVal;

		newEnemy->SetActorRotation(startingRot);
		//reduceEnemySpawningTime();
	}

	return newEnemy;
}

const EnemySubTypes AEnemyStart::generateGruntSubtype()
{
	return (EnemySubTypes)FMath::RandRange(0, (uint8)EnemyGruntTypes::ENEMYGRUNTTYPESMAX - 1);
}

const EnemySubTypes AEnemyStart::generateBossSubtype()
{
	return (EnemySubTypes)FMath::RandRange((uint8)EnemyGruntTypes::ENEMYGRUNTTYPESMAX, (uint8)EnemyBossTypes::ENEMYBOSSTYPESMAX);
}

FEnemyStatsPack AEnemyStart::generateEnemyStats(const EnemySubTypes& enemyType)
{
	FEnemyStatsPack generatedEnemyStats;
	generatedEnemyStats.health = 10;
	generatedEnemyStats.speed = 2;
	generatedEnemyStats.damageCanInflict = 10;
	generatedEnemyStats.attackRate = 2.0f;
	generatedEnemyStats.defense = 1.0f;

	switch (enemyType)
	{
	case EnemySubTypes::ENEMYGRUNTTYPE1:
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::STANDARD;
		generatedEnemyStats.weakAgainst = StrengthAndWeaknessType::NONE;
		generatedEnemyStats.currencyValue = 10;
		break;

	case EnemySubTypes::ENEMYGRUNTTYPE2:
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::FIRE;
		generatedEnemyStats.weakAgainst = StrengthAndWeaknessType::WATER;
		generatedEnemyStats.currencyValue = 20;
		break;

	case EnemySubTypes::ENEMYGRUNTTYPE3:
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::CHILLI;
		generatedEnemyStats.weakAgainst = StrengthAndWeaknessType::SOYAMILK;
		generatedEnemyStats.currencyValue = 30;
		break;

	case EnemySubTypes::ENEMYBOSSTYPE1:
		generatedEnemyStats.health = 50;
		generatedEnemyStats.currencyValue = 50;
		generatedEnemyStats.speed = 5;
		generatedEnemyStats.damageCanInflict = 20;
		generatedEnemyStats.defense = 2.0f;
		generatedEnemyStats.attackRate = 4.0f;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::STANDARD;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::NONE;
		break;

	case EnemySubTypes::ENEMYBOSSTYPE2:
		generatedEnemyStats.health = 50;
		generatedEnemyStats.currencyValue = 60;
		generatedEnemyStats.speed = 5;
		generatedEnemyStats.damageCanInflict = 20;
		generatedEnemyStats.defense = 2.0f;
		generatedEnemyStats.attackRate = 6.0f;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::FIRE;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::WATER;
		break;

	case EnemySubTypes::ENEMYBOSSTYPE3:
		generatedEnemyStats.health = 50;
		generatedEnemyStats.currencyValue = 70;
		generatedEnemyStats.speed = 5;
		generatedEnemyStats.damageCanInflict = 20;
		generatedEnemyStats.defense = 2.0f;
		generatedEnemyStats.attackRate = 10.0f;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::CHILLI;
		generatedEnemyStats.strongAgainst = StrengthAndWeaknessType::SOYAMILK;
		break;
	}

	return generatedEnemyStats;
}

const FVector AEnemyStart::getEnemySpawnLocation()
{
	return spawnLocation;
}

void AEnemyStart::setEnemySpawnLocation(const FVector& newSpawnLocation)
{
	spawnLocation = newSpawnLocation;
}

void AEnemyStart::inflateEnemyStats(FEnemyStatsPack& stats)
{
	int tmpCompletionRate = (numEnemiesSpawned % maxNunberOfEnemies) * 10;
	int increaseVal = statIncreaser * tmpCompletionRate;

	if (tmpCompletionRate >= 30  && tmpCompletionRate <= 80)
	{
		stats.health += increaseVal;
		stats.defense += increaseVal;
		stats.speed += increaseVal;
	}
}

void AEnemyStart::decativateAllEnemies()
{
	for (auto& enemy : enemies)
	{
		enemy->setIsAlive(false);
	}
}

void AEnemyStart::setPlayerCurrency(UPlayerCurrency* newPlayerCurrency)
{
	playerCurrency = newPlayerCurrency;
}

void AEnemyStart::setEnemyStartStats(int numEnemiesToSpawn, int maxNumEnemiesAlive)
{
	maxNunberOfEnemies = numEnemiesToSpawn;
	maxAlive = maxNumEnemiesAlive;
}

void AEnemyStart::setupEnemyStart(int numEnemiesToSpawn, int maxNumEnemiesAlive)
{
	maxNunberOfEnemies = numEnemiesToSpawn;
	maxAlive = maxNumEnemiesAlive;
	//enemyRoute = enemyRouteVal;	
	// the enemy route includes the enemy start tile so we should remove the 
	// first entry in the route ready for when we pass this to the enemies

	if (enemyRoutes.empty())
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy Start Object at %d,%d does not have have routes"),tilePosition.x, tilePosition.y);
	}

	for (auto &route : enemyRoutes)
	{
		route.pop();
	}

	// currently we're only set up to have one active enemy route 
	// so at this stage if we have more than then we need to pick one

	std::stack<Pair> selectedRoute;

	if (enemyRoutes.size() > 0)
	{
		int selectedRouteIndex = FMath::RandRange(0, enemyRoutes.size() - 1);
		selectedRoute = enemyRoutes[selectedRouteIndex];
	}
	else
	{
		selectedRoute = enemyRoutes[0];
	}

	enemyRoute = getManager()->generateEnemyRoute(selectedRoute);
	
	//enemyRoute.RemoveAt(0);
}

TArray<FPair> AEnemyStart::getEnemyRoute()
{
	return enemyRoute;
}

TArray<FPair> AEnemyStart::getFullEnemyRoute()
{
	return enemyRoute;
}

FPair AEnemyStart::getEnemyRouteStart()
{
	return enemyRoute[0];
}

//void AEnemyStart::setManager(AAMapManager* managerPtr)
//{
//	setManagerPtr(managerPtr);
//}

void AEnemyStart::removeRouteElementByIndex(int index)
{
	enemyRoute.RemoveAt(index);
}

void AEnemyStart::setEnemyRoutes(std::vector<std::stack<Pair>> routes)
{
	enemyRoutes = routes;
}