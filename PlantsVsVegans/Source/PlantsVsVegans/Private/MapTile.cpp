// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "MapTile.h"

// Sets default values
AMapTile::AMapTile()
	: manager(nullptr)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	tileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));
	tileMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);
	isPath = false;
	tilePosition = FPair(-1, -1);
	//defenseActor = nullptr;
}

// Sets default values
AMapTile::AMapTile(AAMapManager* mngr, const bool& isPathVal)
	: manager(mngr)
{
	isPath = isPathVal;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	tileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("<Mesh>"));
	tileMesh->AttachToComponent(root, FAttachmentTransformRules::KeepRelativeTransform);

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//defenseActor = nullptr;
	tilePosition = FPair(-1, -1);

}


// Called when the game starts or when spawned
void AMapTile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMapTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

AAMapManager* AMapTile::getManager()
{
	return manager;
}

const bool AMapTile::getIsPath()
{
	return isPath;
}

void AMapTile::setManagerPtr(AAMapManager* managerPtr)
{
	manager = managerPtr;
}

const FVector AMapTile::getAdjustedLocation() const
{
	FVector tmp = this->GetActorLocation();
	tmp.Z = 100;
	return tmp;
}

const bool AMapTile::getIsLoadBalancer()
{
	return isLoadBalancer;
}

void AMapTile::setTilePos(FPair newPos)
{
	tilePosition = newPos;
}

FPair AMapTile::getTilePosition()
{
	return tilePosition;
}