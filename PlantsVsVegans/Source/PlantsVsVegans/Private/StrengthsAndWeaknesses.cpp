// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "StrengthsAndWeaknesses.h"

// Sets default values for this component's properties
UStrengthsAndWeaknesses::UStrengthsAndWeaknesses()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UStrengthsAndWeaknesses::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UStrengthsAndWeaknesses::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

Effectiveness UStrengthsAndWeaknesses::resolveBulletEnemyCollision(StrengthAndWeaknessType enemyStrong, StrengthAndWeaknessType enemyWeak, StrengthAndWeaknessType bulletType)
{
	if (bulletType == enemyStrong)
	{
		return Effectiveness::WEAK;
	}
	else if (bulletType == enemyWeak)
	{
		return Effectiveness::STRONG;
	}
	else 
	{
		return Effectiveness::NORMAL;
	}
}