// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "LoadBalancerTile.h"
#include <algorithm>

ALoadBalancerTile::ALoadBalancerTile()
{
	loadBalancerRoutes.reserve(2);
	weightedRoutes.reserve(2);
	routeStats.reserve(2);
	isPath = true;
	isLoadBalancer = true;
	interactedWithAnEnemy = false;
}

ALoadBalancerTile::~ALoadBalancerTile()
{
	for (FRouteStats* routeStat : routeStats)
	{
		delete routeStat;
		routeStat = nullptr;
	}
}

void ALoadBalancerTile::calculateRouteWeights()
{
	// longer routes should have a lower weight than shorter routes (to encorage gameplay)

	// routes that have had more enemies should have a higher weight

	// if routes have the same weight then it should be chosen randomly


}

std::stack<std::pair<int, int>> ALoadBalancerTile::getPreferedRoute()
{
	return preferedRoute;
}

void ALoadBalancerTile::addRoute(std::stack<std::pair<int, int>> newRoute)
{
	loadBalancerRoutes.push_back(newRoute);
}

void ALoadBalancerTile::setLoadBalanerRoutes(std::vector<std::stack<Pair>> routes)
{
	loadBalancerRoutes = routes;
}

void ALoadBalancerTile::setupWeightedRoutes()
{
	// for each route setup the weightings so that later we can decide which route to pick
	for (auto& param : loadBalancerRoutes)
	{
		weightedRoutes.push_back(FLoadBalancerRouteStruct(FRouteStats(param.size()-1), param));
	}
	
	int startingRouteId = FMath::RandRange(0, weightedRoutes.size()-1);
	preferedRoute = weightedRoutes[startingRouteId].loadBalancerRoute;
}

void ALoadBalancerTile::updatePreferredRoute()
{
	int lowestWeight = INT_MAX;

	for (const auto& param : weightedRoutes)
	{
		if (param.routeStats.routeWeight < lowestWeight)
		{
			lowestWeight = param.routeStats.routeWeight;
			preferedRoute = param.loadBalancerRoute;
		}
	}

}

TArray<FPair> ALoadBalancerTile::generateEnemyRouteByWeight()
{
	TArray<FPair> weightedRoute;

	// first we need to find the route that currently has the lowest weight
	const auto lowestWeightRouteIter = std::min_element(weightedRoutes.begin(), weightedRoutes.end(), []
	(FLoadBalancerRouteStruct statsA, FLoadBalancerRouteStruct statsB) 
	{
		return statsA.routeStats.routeWeight < statsB.routeStats.routeWeight; 
	});

	//Then we convert the route to a TArray so it's compatible with unreal
	auto routeTmp = this->getManager()->generateEnemyRoute(lowestWeightRouteIter->loadBalancerRoute);
	weightedRoute = routeTmp;

	// Finally we return the weighted route
	return weightedRoute;
}

TArray<FPair> ALoadBalancerTile::generateEnemyRouteRandom()
{
	int randomRouteId = FMath::RandRange(0, weightedRoutes.size()-1);
	TArray<FPair> randomRoute;
	auto routeTmp = weightedRoutes[randomRouteId].loadBalancerRoute;
	randomRoute = this->getManager()->generateEnemyRoute(routeTmp);

	weightedRoutes[randomRouteId].routeStats.numEnemiesSent += 1;
	weightedRoutes[randomRouteId].routeStats.routeWeight += 2;

	updatePreferredRoute();

	return randomRoute;

}

//std::stack<std::pair<int, int>> ALoadBalancerTile::findLowestWeightRoute()
//{
//	std::sort(weightedRoutes.begin(), weightedRoutes.end(), [&](FLoadBalancerRouteStruct lbrs) {})
//}