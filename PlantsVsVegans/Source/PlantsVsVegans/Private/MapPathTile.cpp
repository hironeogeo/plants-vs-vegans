// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "MapPathTile.h"

AMapPathTile::AMapPathTile()
{
	isPath = true;
}

// Called when the game starts or when spawned
void AMapPathTile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMapPathTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
