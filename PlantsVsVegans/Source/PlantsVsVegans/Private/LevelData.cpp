// These files and their contents are the property of George Adcock and Alex Fenwick of Steel Rhino Games 2020


#include "LevelData.h"

bool ULevelData::IsLevelDataValid()const
{
	return
		(MapHeight > 0) &&
		(MapWidth > 0) &&
		// currently we can only support square maps 
		(MapHeight == MapWidth) &&
		(MapString != "") &&
		(MapString.Len() == MapHeight * MapWidth);
}