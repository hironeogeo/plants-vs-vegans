// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PlantsVsVegans.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PlantsVsVegans, "PlantsVsVegans" );
