#!/bin/bash

# Example usage:
#   BUILD_CONFIG=Development ./tools/build.sh

clear

set -eu

SCRIPT_DIR="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
source "${SCRIPT_DIR}/unreal-env"
PROJECT_DIR="${SCRIPT_DIR}/.."
#source "${PROJECT_DIR}/env/unreal-env"

ADDITIONAL_ARGS=()
if [[ "${BUILD_SANITIZER:-}" == 1 ]]; then
    if [[ -z "${SANITIZER_TYPE:-}" ]]; then
      echo SANITIZER_TYPE is not set
      exit 1
    fi
    ADDITIONAL_ARGS+=("-UbtArgs=\"-Enable${SANITIZER_TYPE}\"")
fi

exec "${UNREAL_BUILD_TOOL_PATH:-${PROJECT_DIR}/../../../work/dev/jugo/UnrealEngine/Engine/Build/BatchFiles/RunUAT.sh}" \
				BuildCookRun \
				-clientconfig=${BUILD_CONFIG:-Development} -serverconfig=${BUILD_CONFIG:-Development} \
				-project=${PROJECT_DIR}/PlantsVsVegans.uproject \
				-noP4 -build -cook -allmaps -CrashReporter -unversionedcookedcontent -compressed -stage -prereqs -pak -archive \
				-ddc=DerivedDataBackendGraph \
				-archivedirectory=${PROJECT_DIR}/dist \
				-platform=Linux \
				-target=PlantsVsVegans \
				"${ADDITIONAL_ARGS[@]}"
