#!/bin/bash

set -e
(
    SCRIPT_DIR="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
    . ${SCRIPT_DIR}/common.sh
    . ${SCRIPT_DIR}/unreal-env

    function usage {
        green Run unreal
        echo
        echo "Usage: $(basename $0) [-hs] [-e EVENT_NAME]" 2>&1
        echo 'Either runs the editor or a pre built standalone client after using tools/build.sh'
        echo '   -e EVENT_NAME The name of the event environment variable file'
        echo '   -p Enable pixel stream - standalone only'
        echo '   -s Run standalone client'
    }

    while getopts "hs" option; do
        case $option in
            s)
                RUN_STANDALONE=1
                ;;
            h)
                usage
                exit 0
                ;;
            ?)
                red Invalid option: -${OPTARG}
                usage
                exit 1
                ;;
        esac
    done

    # getopts doesn't strip the end-of-options delimiter so we'll shift past it manually
    if [[ "$1" == "--" ]]; then
        shift;
    fi

    if [[ "${RUN_STANDALONE}" == 1 ]]; then
        ${PROJECT_DIR}/dist/Linux/PlantsVsVegans/Binaries/Linux/PlantsVsVegans ${UE_URL_NO_PS} -unattended -AudioMixer -ForceRes -ResX=1920 -ResY=1080 -trace=default,memory
    else
        # Run the Unreal Editor
        exec "${UNREAL_ENGINE_ROOT}/Binaries/Linux/UnrealEditor" "$PROJECT_DIR/PlantsVsVegans.uproject" $@
    fi
)
