#!/bin/bash

PROJECT_DIR="${SCRIPT_DIR}/.."

set -e

# Escape codes for colourised text output
GREEN='\e[32m'
BLUE='\e[34m'
RED='\e[31m'
YELLOW='\e[33m'
NO_COLOUR='\e[0m'

# Helpers for printing colourised text
function green() {
    echo -e ${GREEN}$@${NO_COLOUR}
}

function blue() {
    echo -e ${BLUE}$@${NO_COLOUR}
}

function red() {
    echo -e ${RED}$@${NO_COLOUR}
}

function yellow() {
    echo -e ${YELLOW}$@${NO_COLOUR}
}

