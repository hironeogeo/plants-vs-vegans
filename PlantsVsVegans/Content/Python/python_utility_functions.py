import unreal

# Gets the selected actors from the opened level and prints to console
def getSelectedLevelActors():
    EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)

    selectedActors = EAS.get_selected_level_actors()

    for selectedActor in selectedActors:
        print(selectedActor)

# Gets a list of the actorCompoents in the level that have a name that contains componentName
# returns a list of actorsComponents or an empty list
# e.g getAllLevelActorComponentsByName("Tunnel")
def getAllLevelActorComponentsByName(componentName: str = "", debug: bool = False):
    # required for 5.3 as old functionality (EAS = unreal.EditorActorSubsystem() will be deprecated)
    EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)

    actorComponents = EAS.get_all_level_actors_components()

    targetedComponents = []

    for actorComponent in actorComponents:
        name = actorComponent.get_name()
        if componentName in name:
            targetedComponents.append(actorComponent)
            if debug:
                print(actorComponent.get_name())
    return targetedComponents

# Gets a list of the actorCompoents in the level that have a class name that contains componentClassName
# returns a list of actorComponents or an empty list
# e.g getAllLevelActorComponentsByClassName("BrandedMeshComponent")
def getAllLevelActorComponentsByClassName(componentClassName: str = "", debug: bool = False):
    # required for 5.3 as old functionality (EAS = unreal.EditorActorSubsystem() will be deprecated)
    EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)

    allActorComponents = EAS.get_all_level_actors_components()

    targetedComponents = []

    for actorComponent in allActorComponents:
        className = actorComponent.get_class()
        if componentClassName in str(className):
            targetedComponents.append(actorComponent)
            if debug:
                print(actorComponent.get_name())
    return targetedComponents

# Selects the actors in the level that have a component with the class name that contains className
# returns a list of actors or an empty list
# e.g getActorsThatHaveComponentClass("BrandedMeshComponent")
def getActorsThatHaveComponentClass(className: str = "", debug: bool = False):
    components = getAllLevelActorComponentsByClassName(className, debug)

    actors = []

    for component in components:
        actors.append(component.get_owner())

    if debug:
        for actor in actors:
            print(actor.get_name())

    if (len(actors) > 0):
        EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)
        EAS.set_selected_level_actors(actors)
    else:
        print ("No actors found with that component")

# Selects the actors in the level that have a component with the name that contains componentName
# returns a list of actors or an empty list
# e.g getActorsThatHaveComponentName("Tunnel")
def getActorsThatHaveComponentName(componentName: str = "", debug: bool = False):
    components = getAllLevelActorComponentsByName(componentName, debug)

    actors = []

    for component in components:
        actor = component.get_owner()
        actors.append(actor)

    if debug:
        for actor in actors:
            print(actor.get_name())

    if (len(actors) > 0):
        EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)
        EAS.set_selected_level_actors(actors)
    else:
        print ("No actors found with that component")

def getAllComponetsByClassName(componentClassName: str = ""):
    # required for 5.3 as old functionality (EAS = unreal.EditorActorSubsystem() will be deprecated)
    EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)

    allActorComponents = EAS.get_all_level_actors_components()

    for actorComponent in allActorComponents:
        className = actorComponent.get_class()
        if componentClassName in str(className):
            print(className.get_name())

def countComponentsByClassName(componentClassName: str = ""):
    # required for 5.3 as old functionality (EAS = unreal.EditorActorSubsystem() will be deprecated)
    EAS = unreal.get_editor_subsystem(unreal.EditorActorSubsystem)

    allActorComponents = EAS.get_all_level_actors_components()

    classCounter = {}

    for actorComponent in allActorComponents:
        className = actorComponent.get_class()

        if (componentClassName in str(className)):
            if (className.get_name() in classCounter):
                classCounter[className.get_name()] += 1
            else:
                classCounter[className.get_name()] = 1

    print(classCounter)