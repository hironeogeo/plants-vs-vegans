# Python Intergration Setup

The aim of this wiki is to document how to get your project setup with python editor scripting.

This document is a collection of my own experiences content from some other good sources I'll include at the bottom

## Plugins
If you go to the extensions and then to the scripting section. The plugins we need enabled are 
- Editor Scripting Utilities

- Python Editor Script Plugin

![Alt text](image.png)

## Editor Preferences
If you search for python in the Project settings you need to have the following enabled
- Developer Mode

- Enable Content Browser Integration

![Alt text](image-2.png)


## Project Settings

If you search for python in the Project settings you need to have the following enabled
- Isolate Interpreter Environment
- Developer Mode

Then the location you want to store the python files need to be added to the additional paths

![Alt text](image-1.png)


## VS Code Integration
This is the IDE I was using at the time but you can do this for other IDE's if you want.

The above steps will generate a python stub file, that is basically a condensed version of all the python functionality. It's this stub file that we need to point VS Code at to enable autocomplete and intelisense.

In VS Code if you go to file -> preferences -> settings

Search for python, then click edit in settings for Auto Complete Extra Paths

![Alt text](image-3.png)

you then need to add the path to your stub file to these 2 sections

```json
"python.autoComplete.extraPaths": [
        "~/Dev/plants-vs-vegans/PlantsVsVegans/Intermediate/PythonStub",
    ],
"python.analysis.extraPaths": [
        "~/Dev/plants-vs-vegans/PlantsVsVegans/Intermediate/PythonStub",
    ],
```

Then you should be able to use VS Code to develop and maintain your python scripts.

## Using Python in the Unreal Editor

in order to use python in unreal 

We run thse scripts through the command line interface built into the output log (make sure it's set to Python rather than CMD)

![Alt text](image-4.png)

if you want to see the paths that unreal is currently looking at for python files you can do so with

```python
import sys
for path in sys.path:
    print(path)
```
^ I find 4 spaces is usually right for the indentation you need but you can do it on 1 line as well if you want 

```python
for path in sys.path:    print (path)
```

These paths are where unreal is able to detect python files so make sure the scripts you're importing are from one of these dirs

You can import files with aliases

```python
import python_utility_functions as p
```

Then use them with 

```python
p.getSelectedLevelActors()
```

If you change a file after it's been imported you'll have to reload it in unreal to have the new changes.

```python
from importlib import reload
reload(NAME-OF-FILE)
file.UpdatedMethod()
```

### Additional good content

- https://docs.unrealengine.com/5.2/en-US/scripting-the-unreal-editor-using-python/

- https://dev.epicgames.com/community/learning/courses/wk4/utilizing-python-for-editor-scripting-in-unreal-engine/Ebnj/unreal-engine-installation-and-setup

- https://www.youtube.com/watch?v=0guOMTiwmhk

